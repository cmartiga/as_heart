# Role of PTBP1 in cardiac disease
In this repository we provide data and scripts required for the analysis performed
for the phenotypic and molecular characterization of hearts from mice over-expressing
PTBP1 using an adenoassociated virus type 9 (AAV9).

### Download repository
Download the repository using git
```bash
git clone git@bitbucket.org:cmartiga/ptbp1.git
```

Create a conda environment and install the required R and python libraries using
the package provided in the repository

```bash
conda create -n ptbp1 r=3.5.1
conda activate ptbp1
conda install matplotlib statsmodels seaborn numpy pandas scipy
Rscripts R_scripts/install_libraries.R
```

Add the repository the PYTHONPATH by adding the following line to the activate script
NOTE: this will have to be done every time the environment is activated
```bash
export PYTHONPATH=$PYTHONPATH:repository_path
```

## Phenotypic characterization

All phenotypic characterization measures are provided in the data directory

- qPCR quantifications for heart function and fibrosis markers and PTBP1
- qPCR quantifications for alternative splicing changes observed in TAC and MI that are putatively regulated by PTBP1
- Echocardiographic parameters
- Fibrosis measures from histological analysis

Statistical analysis is performed using linear regression to correct for batch effects or experimental variability when needed, as two independent PTBP1 over-expression experiments have been performed. To run the analysis

```bash
python scripts/analysis_ptbp1_aav9.py
```

### PTBP1 over-expression in independent TAC and MI experiments

```bash
python scripts/figure_ptbp1_in_TAC_MI.py
```

![TAC-MI validation](https://bitbucket.org/cmartiga/ptbp1/raw/master/plots/ptbp1_validation_tac_mi.png)


### Characterization of mice over-expressing PTBP1 in the heart

```bash
python scripts/figure_ptbp1_oe.py
```

![TAC-MI validation](https://bitbucket.org/cmartiga/ptbp1/raw/master/plots/ptbp1_characterization.png)


## RNA-seq analysis
### Processed data
(Raw data)[GEO repository] was processed using [vast-tools](https://github.com/vastgroup/vast-tools), providing gene counts matrices for differential expression analysis and and results from differential splicing analysis, that are provided directly in the repository as starting point in the data directory. 

### Differential gene expression analysis

Differential expression analysis was performed using limma-voom normalization as [previously described](https://www.bioconductor.org/packages/devel/workflows/vignettes/RNAseq123/inst/doc/limmaWorkflow.html#software-and-code-used). To reproduce the analysis run:

```bash
Rscript R_scripts/expression_analysis.R
```

![Gene expression PCA](https://bitbucket.org/cmartiga/ptbp1/raw/master/plots/GE.PCA.png)

We then compared expression and splicing changes induced by PTBP1 over-expression with those taking place in TAC and MI mouse models

```bash
python scripts/figure_ptbp1_rnaseq.py
```

![RNA-seq results](https://bitbucket.org/cmartiga/ptbp1/raw/master/plots/figure_rna_seq.png)


### Functional analysis
We use GO and KEGG gene sets downloaded from [Enrichr](http://amp.pharm.mssm.edu/Enrichr/),
which are located already in the external_data directory, together with
gene names to gene ids mapping downloaded from [BioMart](https://www.ensembl.org/biomart/martview/c8653dc0bf93a580115776eebcd89ee6)
  
Functional analysis can be run with the following scripts. Resulting files are
stored in the functional_analysis directory, which are also provided. This part
can therefore be skipped also.

```bash
python scripts/functional_analysis.py
```

Which can be plotted with the following script

```bash
python scripts/figure_functional_analysis.py
```

![Functional analysis](https://bitbucket.org/cmartiga/ptbp1/raw/master/plots/figure_functional_analysis.png)


### CLiP binding sites analysis
To investigate whether the observed splicing changes are mediated by PTBP1 binding directly to the regulated exons, we plot the distribution of experimentally determined in vivo binding sites from CLiP-seq data across potential regulatory regions relative to the putatively regulated exons. 

```bash
python scripts/figure_profile.py
```

![PTBP1 binding profile](https://bitbucket.org/cmartiga/ptbp1/raw/master/plots/profiles_ptbp1.png)


