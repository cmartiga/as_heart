#!/usr/bin/env python

from setuptools import setup, find_packages
from AS_heart.settings import VERSION


def main():
    description = 'Alternative splicing data analysis in the heart'
    setup(
        name='AS_heart',
        version=VERSION,
        description=description,
        author_email='cmarti@cnic.es',
        url='https://bitbucket.org/cmartiga/AS_quant',
        packages=find_packages(),
        include_package_data=True,
        entry_points={
            'console_scripts': []},
        install_requires=['numpy', 'pandas', 'scipy', 'pysam', 'networkx',
                          'seaborn', 'matplotlib', 'r2py'],
        platforms='ALL',
        keywords=['bioinformatics', 'alternative splicing', 'RNA-Seq'],
        classifiers=[
            "Programming Language :: Python :: 3",
            'Intended Audience :: Science/Research',
            "License :: OSI Approved :: MIT License",
            "Operating System :: OS Independent",
        ],
    )
    return


if __name__ == '__main__':
    main()
