#!/usr/bin/env python

from os.path import join

from scipy.stats.stats import fisher_exact
import pandas as pd

from scripts.settings import DATA_DIR, CONDITIONS
from scripts.utils import calc_contingency_table


if __name__ == '__main__':
    fpath = join(DATA_DIR, 'alternative_gene_data.csv')
    gene_data = pd.read_csv(fpath, index_col=0)
    
    results = []
    for c in CONDITIONS:
        x = gene_data['{}_DAS'.format(c)].astype(int)
        y = gene_data['{}_DEG'.format(c)].fillna(False).astype(int)
        table = calc_contingency_table(x, y)
        ors, pvalue = fisher_exact(table, alternative='greater')
        res = {'Contrast': c,
               'Differentially AS': table[0,0] / table[:,0].sum(),
               'No-change': table[0,1] / table[:,1].sum(),
               'OR': ors, 'pvalue': pvalue,
               'N_DAS': table[:,0].sum(), 'N_noDAS': table[:,1].sum()}
        results.append(res)
    results = pd.DataFrame(results)
    
    fpath = join(DATA_DIR, 'AS_GE_overlap.csv')
    results.to_csv(fpath)
