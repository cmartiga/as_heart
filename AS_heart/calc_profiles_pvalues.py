#!/usr/bin/env python
from os.path import join
import numpy as np
import pandas as pd

from scipy.stats.stats import fisher_exact

from scripts.settings import DEF_REGIONS, CLIP_DIR, DATA_DIR
from scripts.utils import (add_das_categories, load_vast_splicing,
                           calc_contingency_table)

            
def calc_pvalues(splc_data, regions=DEF_REGIONS):
    pvalues = {}
    groups = ['Included', 'Skipped']
    for region in regions:
        fpath = join(CLIP_DIR, 'ptbp1.profile.{}'.format(region))
        sites = pd.read_csv(fpath, sep='\t', index_col=0)
        common_ids = np.intersect1d(sites.index, splc_data['event_id'])
        sites = sites.reindex(common_ids)
        sd = splc_data.reindex(common_ids)
        
        if region.split('.')[1] == 'r':
            sites = sites[sites.columns[::-1]]

        pvalues = {g: [] for g in groups}
        for i in range(1, sites.shape[1]):
            presence = np.any(sites.iloc[:,:i], axis=1).astype(int)
            for group in groups:
                g = (sd['Group'] == group).astype(int)
                table = calc_contingency_table(presence, g)
                pvalue = fisher_exact(table, alternative='two-sided')[1]
                pvalues[group].append(pvalue)
        
        for g in groups:
            if region.split('.')[1] == 'r':
                pvalues[g] = pvalues[g][::-1]
            yield(region, g, np.min(pvalues[g]), np.argmin(pvalues[g]))


if __name__ == '__main__':
    splc_data = load_vast_splicing()
    add_das_categories(splc_data, dpsi_threshold=0.1, p_threshold=-1)
    data = calc_pvalues(splc_data, regions=DEF_REGIONS)
    
    out_fpath = join(DATA_DIR, 'profiles.pvalues.csv')
    with open(out_fpath, 'w') as fhand:
        fhand.write('Region,Group,p-value,Position\n')
        for region, group, pvalue, pos in data:
            print(region, group, pvalue, pos)
            fhand.write('{},{},{},{}\n'.format(region, group, pvalue, pos))
    