#!/usr/bin/env python

from csv import DictReader
from os.path import join


import numpy as np
import pandas as pd
from AS_heart.settings import (MOUSE_PFAM_FPATH, HUMAN_PFAM_FPATH,
                               EXONS_DOMAINS_FPATH, DATA_DIR,
                               DDI_INTERACTIONS_FPATH, VIDAL_PPI_FPATH,
                               INTACT_FPATH)
from AS_heart.ppi.utils import (EventToGeneSymbol, GeneIdToSymbol, ContrastGroups,
                                load_gene_domains, load_prot2gene, load_exon_domains,
                                EventToGeneId, OrthologsMapper, calc_edges_loads)


def get_vidal_ppi_data(expressed_genes):
    df = pd.read_csv(VIDAL_PPI_FPATH)
    fieldnames = ['Gene_Symbol', 'Interactor_Symbol', 'Interaction_Found']

    genes = np.unique(df['Gene_Symbol'])
    ppi_change = {gene: {'total': 0, 'changed': 0} for gene in genes}
    extended = []

    network = df.groupby(fieldnames[:2])[fieldnames[-1]]
    for (gene1, gene2), interactions in network:
        if '-sep' in gene1 or '-sep' in gene2:
            continue
        if gene1 not in expressed_genes or gene2 not in expressed_genes:
            continue
        ppi_change[gene1]['total'] += 1
        interactions = set(interactions)
        change = 'positive' in interactions and 'negative' in interactions
        extended.append([gene1, gene2, change])
        if change:
            ppi_change[gene1]['changed'] += 1

    ppi_change = pd.DataFrame.from_dict(ppi_change, orient='index')
    colnames = ['Gene1', 'Gene2', 'AS_regulated']
    extended = pd.DataFrame(extended, columns=colnames)
    return(ppi_change, extended)


def load_ddis(prot2gene, orthologs):
    df = pd.read_csv(DDI_INTERACTIONS_FPATH)
    for prot1, prot2, ddis in zip(df['Protein_1_UniProt_ID'],
                                  df['Protein_2_UniProt_ID'],
                                  df['Mapping_DDIs']):
        gene1 = orthologs[prot2gene.get(prot1, None)]
        gene2 = orthologs[prot2gene.get(prot2, None)]
        if gene1 is None or gene2 is None:
            continue
        domain_pairs = [d_pair.strip().split('-') for d_pair in ddis.split(',')]
        yield(gene1, gene2, domain_pairs)


def domains_AS_affected(genes, domains, dom2exon, exon2gene, contrast_groups):
    contrasts = contrast_groups.groups_dict.keys()
    record = {contrast: 'No-change' for contrast in contrasts}
    record['AS_regulated'] = False
    exons_ppi = {}

    for domain in domains:
        for exon in dom2exon[domain]:
            exon_gene = exon2gene[exon]
            if exon_gene not in genes:
                continue
            groups = contrast_groups.get_exon_group(exon)
            record['AS_regulated'] = True
            exons_ppi[exon] = True

            for contrast, group in groups.items():
                if group is not None and group != 'No-change':
                    if record[contrast] != 'No-change':
                        record[contrast] = 'Complex'
                    else:
                        record[contrast] = group
    return(record, exons_ppi)


def get_DDI_ppi_data(expressed_gene_ids, prot2gene, orthologs,
                     exon2gene, dom2exon, contrast_groups, gene2symbol):

    data = []
    exons_ppi = {exon: False for exon in contrast_groups.groups.index}

    for gene1, gene2, domains in load_ddis(prot2gene, orthologs):
        if gene1 not in expressed_gene_ids or gene2 not in expressed_gene_ids:
            continue
        domains = set(np.hstack(domains))
        record, exons_data = domains_AS_affected((gene1, gene2), domains,
                                                 dom2exon, exon2gene,
                                                 contrast_groups)
        record['Gene1'] = gene2symbol[gene1]
        record['Gene2'] = gene2symbol[gene2]
        exons_ppi.update(exons_data)
        data.append(record)

    ppi_changes = [exons_ppi[exon] for exon in contrast_groups.groups.index]
    contrast_groups.groups['ppi_change'] = ppi_changes
    data = pd.DataFrame(data)
    return(data)


def _parse_record(record, prefix, orthologs, gene_id_to_symbol):
    parsed = {}
    for interactor, label in [('A', 'Gene1'), ('B', 'Gene2')]:
        fieldname = 'Xref(s) interactor {}'.format(interactor)
        info = record[fieldname]
        items = [orthologs[item.split(':')[-1]] for item in info.split('|')]
        sel_item = [item for item in items
                    if item is not None and prefix in item]
        if len(set(sel_item)) != 1:
            return None
        parsed[label] = gene_id_to_symbol[sel_item[0]]
    parsed['type'] = record['Interaction type(s)'].split('(')[-1].strip(')')
    parsed['confidence'] = float(record['Confidence value(s)'].split(':')[-1])
    return parsed


def _AS_regulated(record, contrasts_data, label='AS'):

    for contrast in contrasts_data.groups.keys():
        g1 = contrasts_data.get_gene_group(record['Gene1'], contrast=contrast)
        g2 = contrasts_data.get_gene_group(record['Gene1'], contrast=contrast)
        if g1 != 'No-change' or g2 != 'No-change':
            record[contrast] = '{}-change'.format(label)
        else:
            record[contrast] = 'No-change'


def load_intact_ppi(prefix, orthologs, gene_id_to_symbol,
                    contrasts_data, expressed_genes, label='AS',
                    filter_assoc=True):
    with open(INTACT_FPATH) as fhand:
        reader = DictReader(fhand, delimiter='\t')
        for record in reader:
            record = _parse_record(record, prefix, orthologs,
                                   gene_id_to_symbol)
            if record is None or (filter_assoc and
                                  record['type'] == 'association'):
                continue
            g1, g2 = record['Gene1'], record['Gene2']
            if (g1 is None or g2 is None or
                    g1 not in expressed_genes or g2 not in expressed_genes):
                continue
            _AS_regulated(record, contrasts_data, label=label)
            yield(record)


if __name__ == '__main__':
    # Load correspondences
    event_to_gene = EventToGeneSymbol()
    gene_id_to_symbol = GeneIdToSymbol()
    orthologs = OrthologsMapper()

    # Get expressed genes
    fpath = join(DATA_DIR, 'ED.differential_expression.csv')
    expressed_gene_ids = set(pd.read_csv(fpath, index_col=0).index)
    expressed_genes_symbols = set([gene_id_to_symbol[gene]
                                   for gene in expressed_gene_ids])

#     # Analysis with Vidal data
#     regulatory_layers = {'AS': ('_grouping.tab', event_to_gene),
#                          'GE': ('.GE.grouping.tab', gene_id_to_symbol)}
# 
#     for label, (suffix, mapping_func) in regulatory_layers.items():
#         data, extended = get_vidal_ppi_data(expressed_genes_symbols)
#         
#         # Load grouping for each condition
#         contrasts_data = ContrastGroups(suffix=suffix)
#         contrasts_data.calc_gene_groups(mapping_func,
#                                         change_label='{}-change'.format(label))
# 
#         # Merge network with observed changes
#         groups = pd.DataFrame([contrasts_data.get_gene_group(gene)
#                                for gene in data.index], index=data.index)
#         data_as = pd.concat([data, groups], axis=1)
#         extended_groups = pd.DataFrame([contrasts_data.get_gene_group(gene)
#                                         for gene in extended['Gene1']])
#         data_as.to_csv(join(DATA_DIR, 'vidal_{}_data.csv'.format(label)))
# 
#         extended_as = pd.concat([extended, extended_groups], axis=1)
#         calc_edges_loads(extended_as)
#         fpath = join(DATA_DIR, 'vidal_{}_ppi_data.csv'.format(label))
#         extended_as.to_csv(fpath, index=False)
# 
#         # Get full PPI network from Intact
#         contrasts_data = ContrastGroups(suffix=suffix)
#         contrasts_data.calc_gene_groups(mapping_func,
#                                         change_label='{}-change'.format(label))
#         sp_prefix = 'ENSMUSG'
#         ppi_network = load_intact_ppi(sp_prefix, orthologs=orthologs,
#                                       gene_id_to_symbol=gene_id_to_symbol,
#                                       contrasts_data=contrasts_data,
#                                       expressed_genes=expressed_genes_symbols,
#                                       label=label)
#         data = pd.DataFrame(ppi_network)
#         data['AS_regulated'] = False
#         calc_edges_loads(data)
#         fpath = join(DATA_DIR, 'intact_{}_ppi_data.csv'.format(label))
#         data.to_csv(fpath, index=False)
# 
    # Analysis with DDI data
    gene2dom, dom2gene = load_gene_domains(MOUSE_PFAM_FPATH)
    prot2gene = load_prot2gene(HUMAN_PFAM_FPATH)
    exon2dom, dom2exon = load_exon_domains(EXONS_DOMAINS_FPATH)
    exon2geneid = EventToGeneId()
    contrasts_data = ContrastGroups()
    contrasts_data.calc_gene_groups(event_to_gene)

    # Get data from networks
    ddi_data = get_DDI_ppi_data(expressed_gene_ids, prot2gene, orthologs,
                                exon2geneid, dom2exon, contrasts_data,
                                gene_id_to_symbol)
    calc_edges_loads(ddi_data)
    fpath = join(DATA_DIR, 'ddi_AS_ppi_data.csv')
    ddi_data.to_csv(fpath, index=False)

    sel_rows = [x.startswith('MmuEX') for x in contrasts_data.groups.index]
    contrasts_data = contrasts_data.groups.loc[sel_rows, :]
    fpath = join(DATA_DIR, 'ddi_AS_data.csv')
    contrasts_data.to_csv(fpath)
