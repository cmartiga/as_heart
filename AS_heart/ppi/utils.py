from _collections import defaultdict
from os.path import join, exists

import numpy as np
import pandas as pd
import networkx as nx
from networkx.algorithms.centrality import edge_betweenness_centrality

from AS_heart.settings import (EVENT2GENE_FPATH, ID2SYMBOL_FPATH, CONTRASTS,
                               EVENT2GENEID_FPATH,
                               MOUSE_ORTHOLOGS_FPATH, DATA_DIR)


class IdsMapper(object):
    def init(self, fpath, upper=True, key_idx=0, value_idx=1, sep='\t'):
        self.mapper = {}
        for line in open(fpath):
            row_items = line.strip().split(sep)
            if len(row_items) < 2:
                continue
            value = row_items[value_idx].split()[-1]
            if upper:
                value = value.upper()
            self.mapper[row_items[key_idx]] = value

    def __getitem__(self, key_id):
        return(self.mapper.get(key_id, None))


class EventToGeneSymbol(IdsMapper):
    def __init__(self, upper=True):
        self.fpath = EVENT2GENE_FPATH
        self.init(self.fpath, upper=True, key_idx=0, value_idx=1)


class EventToGeneId(IdsMapper):
    def __init__(self, upper=True):
        self.fpath = EVENT2GENEID_FPATH
        self.init(self.fpath, upper=True, key_idx=0, value_idx=1)


class GeneIdToSymbol(IdsMapper):
    def __init__(self, upper=True):
        self.fpath = ID2SYMBOL_FPATH
        self.init(self.fpath, upper=upper, key_idx=0, value_idx=1)


class OrthologsMapper(IdsMapper):
    def __init__(self, upper=True):
        self.fpath = MOUSE_ORTHOLOGS_FPATH
        self.init(self.fpath, upper=upper, key_idx=0, value_idx=1, sep=',')


class ContrastGroups(object):
    def __init__(self, prefixes=CONTRASTS, suffix='_grouping.tab',
                 index_col=0):
        dfs = []
        for contrast in prefixes:
            fpath = join(DATA_DIR, '{}{}'.format(contrast, suffix))
            if not exists(fpath):
                msg = '{} file not found: skip {} group'.format(fpath,
                                                                contrast)
                print(msg)
                continue
            df = pd.read_csv(fpath, sep='\t', index_col=index_col)
            df.columns = [contrast]
            dfs.append(df)
        self.groups = pd.concat(dfs, axis=1)
        self.groups_dict = self.groups.to_dict(orient='dict')

    def calc_gene_groups(self, event2gene, change_label='AS-change',
                         no_change_label='No-change'):
        genes = set([event2gene[event] for event in self.groups.index])
        self.gene_groups_dict = {contrast: {gene: no_change_label
                                            for gene in genes
                                            if gene is not None}
                                 for contrast in self.groups.columns}
        for contrast, groups in self.groups_dict.items():
            for event, group in groups.items():
                gene = event2gene[event]
                if gene is not None and group != no_change_label:
                    self.gene_groups_dict[contrast][gene] = change_label
        self.gene_groups = pd.DataFrame(self.gene_groups_dict)

    def _get_group(self, identifier, group_dict, contrast=None):
        if contrast is None:
            return({contrast: group_dict[contrast].get(identifier, None)
                    for contrast in self.groups.columns})
        else:
            return(group_dict[contrast].get(identifier, None))

    def get_gene_group(self, gene, contrast=None):
        if not hasattr(self, 'gene_groups_dict'):
            msg = 'Gene groups unkwnown. Call calc_gene_groups method first'
            raise ValueError(msg)
        return(self._get_group(gene, self.gene_groups, contrast=contrast))

    def get_exon_group(self, exon, contrast=None):
        return(self._get_group(exon, self.groups_dict, contrast=contrast))


def filter_common_ids(df1, df2):
    common = np.intersect1d(df1.index, df2.index)
    return(df1.loc[common, :], df2.loc[common, :])


def load_exon_domains(fpath):
    df = pd.read_csv(fpath, sep='\t')
    exon_domains = defaultdict(list)
    domain_exon = defaultdict(list)
    for exon, domains in zip(df['EVENT'], df['A_DOM_PFAM']):
        if isinstance(domains, float):
            continue
        for domain in domains.split(','):
            domain = domain.split('=')[0]
            for domain in [domain[:7], domain[:8]]:
                domain_exon[domain].append(exon)
                exon_domains[exon].append(domain)
    return(exon_domains, domain_exon)


def load_prot2gene(fpath):
    df = pd.read_csv(fpath)
    fieldnames = ['UniProtKB Gene Name ID', 'Gene stable ID']
    df = df[fieldnames].drop_duplicates()
    sel_prots = set(df['UniProtKB Gene Name ID'])
    df = df.loc[[prot in sel_prots
                 for prot in df['UniProtKB Gene Name ID']], :]

    prot2gene = dict(zip(df['UniProtKB Gene Name ID'], df['Gene stable ID']))
    return(prot2gene)


def load_gene_domains(fpath):
    df = pd.read_csv(fpath)
    gene_domains = {}
    domain_genes = defaultdict(list)
    for gene, domains in df.groupby('Gene stable ID')['Pfam domain ID']:
        gene_domains[gene] = domains
        for domain in domains:
            domain_genes[domain].append(gene)
    return(gene_domains, domain_genes)


def df_to_graph(df, sorted_edges=False):
    ppi = nx.Graph()
    for record in df.to_dict(orient='index').values():
        g1, g2 = record['Gene1'], record['Gene2']
        if sorted_edges:
            g1, g2 = sorted([g1, g2])
        ppi.add_edge(g1, g2, **record)
    return(ppi)


def calc_edges_loads(df):
    ppi = df_to_graph(df)
    edges_betweenness = edge_betweenness_centrality(ppi)
    df['edge_load'] = [edges_betweenness.get((gene1, gene2),
                                             edges_betweenness.get((gene2, gene1), np.nan))
                       for gene1, gene2 in zip(df['Gene1'], df['Gene2'])]
