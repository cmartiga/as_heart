#!/usr/bin/env python
from csv import DictReader
from os.path import join, exists

from networkx.algorithms.centrality.load import edge_load

import networkx as nx
import pandas as pd
from plot_utils import init_fig, savefig
import seaborn as sns
from settings import vidal_palette, PPI_DIR, XORDER, PLOTS_DIR, transform


def plot_edges_loads(df, palette, fpath):
    # Plot interactions properties
    fig, axes = init_fig(1, 1, figsize=(3, 3))
    axes = sns.boxplot(x='variable', y='edge_load', hue='value',
                       data=df, order=XORDER, palette=palette, ax=axes,
                       showfliers=False)
    axes.set_xlabel('Contrast')
    axes.set_ylabel('Edge Betweenness')
    axes.legend(loc=2)
    sns.despine()
    savefig(fig, fpath)


def plot_ppi_p_changes(df, fpath):
    fig, axes = init_fig(1, 1, figsize=(3, 3))
    axes = sns.barplot(x='Contrast', y='value', hue='variable',
                       data=df, order=XORDER, n_boot=0, palette=vidal_palette,
                       ax=axes, linewidth=1.5, edgecolor='black')
    axes.set_xlabel('Contrast')
    axes.set_ylabel('Pr(AS-PPI change)')
    axes.legend(loc=2)
    axes.set_ylim((0, 1.2))
    sns.despine()
    savefig(fig, fpath)


def calc_edges_loads(ppi_fpath):
    ppi = nx.Graph()
    with open(ppi_fpath) as fhand:
        reader = DictReader(fhand)
        for record in reader:
            ppi.add_edge(record['Gene1'], record['Gene2'], record)
    return(edge_load(ppi))


if __name__ == '__main__':
    palette = vidal_palette

    for flabel, label in [('as', 'AS'), ('expr', 'GE')]:
        # Load interactions information
        fpath = join(PPI_DIR, 'vidal_{}_enrichment_results.csv'.format(flabel))
        df = pd.read_csv(fpath)

        # Format table for plotting
        id_vars = ['Contrast']
        if 'pvalue_alt' in df.columns:
            id_vars.extend(['pvalue_alt', 'pvalue_change'])
            var_labels = {'Pnochange': 'No {}'.format(label),
                          'Palt': 'No {}-change'.format(label),
                          'Pchange': '{}-change'.format(label)}
        else:
            id_vars.append('pvalue')
            var_labels = {'Pnochange': 'No {}-change'.format(label),
                          'Pchange': '{}-change'.format(label)}

        df = pd.melt(df, id_vars=id_vars)
        df['variable'] = [var_labels[x] for x in df['variable']]
        df['Contrast'] = [transform.get(x, x) for x in df['Contrast']]

        # Plot changes probabilities
        fpath = join(PLOTS_DIR,
                     'vidal_{}_enrichment_results.svg'.format(label))
        plot_ppi_p_changes(df, fpath)

        # Load networks data
        ppi_fpath = join(PPI_DIR, 'vidal_{}_ppi_data.csv'.format(label))
        if not exists(fpath):
            continue
        edges_betweenness = calc_edges_loads(ppi_fpath)

        # Merge changes and edge betweenness
        df = pd.read_csv(ppi_fpath)
        df['edge_load'] = [edges_betweenness[(gene1, gene2)]
                           for gene1, gene2 in zip(df['Gene1'], df['Gene2'])]
        df.columns = [transform.get(col, col) for col in df.columns]
        df = pd.melt(df, id_vars=['Gene1', 'Gene2', 'edge_load'])
        df['value'] = ['{}-change'.format(label) if value
                       else 'No {}-change'.format(label)
                       for value in df['value']]
        fpath = join(PPI_DIR, 'vidal_{}_ppi_properties.csv'.format(label))
        df.to_csv(fpath)

        fpath = join(PLOTS_DIR, 'vidal_{}_ppi_properties.svg'.format(label))
        plot_edges_loads(df, fpath)
