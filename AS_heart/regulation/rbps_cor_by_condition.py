#!/usr/bin/env python
from itertools import combinations
from os.path import join 

import numpy as np
import pandas as pd
from scipy.stats.stats import pearsonr, ttest_ind

from AS_heart.settings import DATA_DIR, EXT_DATA_DIR


def read_ppi(fpath):
    ppi = []
    with open(fpath) as fhand:
        for line in fhand:
            gene1, gene2 = line.strip().split()
            try:
                gene1 = symbol2genes[gene1].upper()
                gene2 = symbol2genes[gene2].upper()
                ppi.append((gene1, gene2))
            except KeyError:
                continue
    return(ppi)


def calc_rho_null_distrib(df, n=5000):
    null_distrib = []
    combs = []
    df = df.reset_index().drop_duplicates(subset=['index']).set_index('index')
    X = df.transpose()
    for _ in range(n):
        cols = np.random.choice(X.columns, size=2)
        combs.append(cols)
        x = X[cols].dropna()
        if x.shape[0] > 5:
            a, b = x[cols[0]].values, x[cols[1]].values
            if len(a.shape) > 1 or len(b.shape) > 1:
                continue
            rho = pearsonr(a, b)[0]
            null_distrib.append(rho)
    return(combs, null_distrib)


def get_rho_records(gene_pairs, df, mu, sigma, comparison_label, group_label):
    data = []
    X = df
    for gene1, gene2 in gene_pairs:
        cols = [gene1, gene2]
        if gene1 not in df.columns or gene2 not in df.columns:
            continue
        x = X[cols].dropna()
        if x.shape[0] > 5:
            rho, _ = pearsonr(x[gene1], x[gene2])
            data.append({'gene1': gene1, 'gene2': gene2, 'rho': rho,
                         'z': (rho - mu) / sigma, 'condition': comparison_label,
                         'Type': group_label})
    return(data)


def calc_norm_cor_df(design, logcounts, phenotypes, rbps):
    design['runid'] = design.index
    data = []
    for conds in zip(phenotypes, phenotypes[1:]):
        comparison_label = conds[1]
        sel_rows = [x in conds for x in design['Condition']]
        sel_samples = design.index[sel_rows]
        subdf = logcounts[sel_samples]
        rbps_expr = subdf.loc[rbps, :].transpose().dropna()

        combs, null_distrib = calc_rho_null_distrib(subdf, n=5000)
        mu, sigma = np.nanmean(null_distrib), np.nanstd(null_distrib)
        
        for (gene1, gene2), rho in zip(combs, null_distrib):
            data.append({'gene1': gene1, 'gene2': gene2, 'rho': rho,
                         'z': (rho - mu) / sigma, 'condition': comparison_label,
                         'Type': 'Random'})
        data.extend(get_rho_records(combinations(rbps_expr.columns, 2),
                                    rbps_expr, mu, sigma,
                                    comparison_label, group_label='RBPs'))
        data.extend(get_rho_records(ppi, rbps_expr, mu, sigma,
                                    comparison_label, group_label='PPI'))
    data = pd.DataFrame(data)
    return(data)


if __name__ == '__main__':
    phenotypes = ['Embryo', 'Neonatal', 'Adult', 'TAC', 'MI']
    
    # File paths
    g2s_fpath = join(EXT_DATA_DIR, 'gene_ids.gene_symbols.tsv')
    expr_fpath = join(DATA_DIR, 'expression.voom.csv')
    design_fpath = join(DATA_DIR, 'design.csv')
    ppi_fpath = join(EXT_DATA_DIR, 'ppi.txt')
    clip_fpath = join(DATA_DIR, 'clip_merged.bs.csv')
    out_fpath = join(DATA_DIR, 'rbps_condition_correlations.csv')
    
    # Load data
    symbol2genes = dict(tuple(line.strip().split()[:2])
                        for line in open(g2s_fpath))
    counts = pd.read_csv(expr_fpath, index_col=0)
    design = pd.read_csv(design_fpath, index_col=0)
    samples = np.intersect1d(design.index, counts.columns)
    logcounts = np.log(counts[samples] + 1e-6)
    logcounts.index = [symbol2genes.get(gene_id, gene_id).upper()
                       for gene_id in logcounts.index]
    ppi = read_ppi(ppi_fpath)
    binding_df = pd.read_csv(clip_fpath, index_col=0)
    rbps = np.unique([colname.split('.')[0] for colname in binding_df.columns])
    rbps = np.intersect1d(logcounts.index, rbps)

    # Calculate correlations among rbps
    data = calc_norm_cor_df(design, logcounts, phenotypes, rbps)
    data.to_csv(out_fpath)
