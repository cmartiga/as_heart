#!/usr/bin/env python

from _collections import defaultdict
from csv import DictReader
from os.path import join, exists

from matplotlib import cm
from matplotlib.gridspec import GridSpec
from matplotlib.pyplot import colorbar
from pandas.util.testing import isiterable
from scipy.cluster.hierarchy import linkage, dendrogram
from scipy.stats.stats import pearsonr, fisher_exact
from seaborn.matrix import clustermap
from sklearn.grid_search import GridSearchCV
from sklearn.linear_model.coordinate_descent import ElasticNet, ElasticNetCV
from sklearn.linear_model.stochastic_gradient import SGDClassifier
from statistics.base import padjust
from statistics.entropy import calc_MI, calc_H
from utils.plot_utils import make_2d_plot, make_boxplot, repel_labels, init_fig, \
    savefig

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import pandas as pd
import rpy2.robjects as R
import rpy2.robjects.packages as rpackages
import seaborn as sns
import statsmodels.api as sm
import statsmodels.formula.api as smf


RBPS_NAMES = {'NSR100': 'SRRM4', 'QKI': 'QK'}


class SemanticSimilarityCalculator():
    def __init__(self, species_db='org.Hs.eg.db'):
        rpackages.importr('GOSemSim')
        self.ontologies = {}
        self.species_db = species_db
        self.available_measures = ['Jiang', 'Wang', 'Rel', 'Lin', 'Resnik']
        self.available_ontologies = ['MF', 'BP', 'CC']

    def _load_ontology(self, ontology, use_ensembl=True):
        if ontology not in self.available_ontologies:
            msg = '{} not supported: only {}'
            raise ValueError(msg.format(ontology, self.available_ontologies))

        db = R.StrVector([self.species_db])
        ont = R.StrVector([ontology])
        if self.species_db == 'org.At.tair.db':
            keytype = R.StrVector(['TAIR'])
        else:
            keytype = R.StrVector(['ENSEMBL'])

        self.ontologies[ontology] = R.r['godata'](db, ont=ont, keytype=keytype)

    def _check_args(self, measure, ontology):
        if measure not in self.available_measures:
            msg = '{} not supported: only {}'
            raise ValueError(msg.format(measure, self.available_measures))

        if ontology not in self.ontologies:
            self._load_ontology(ontology)

    def calc_ind_sim(self, go1, go2, ontology='BP', measure="Jiang"):
        self._check_args(measure, ontology)
        sim = R.r['goSim'](R.StrVector(go1), R.StrVector(go2),
                           semData=self.ontologies[ontology],
                           measure=R.StrVector([measure]))[0]
        return sim

    def calc_sim_matrix(self, go, ontology='BP', measure="Jiang",
                        one_by_one=False):
        self._check_args(measure, ontology)

        if one_by_one:
            if hasattr(go, 'shape'):
                n = go.shape[0]
            else:
                n = len(go)

            sim = np.zeros((n, n))
            for i in range(n):
                for j in range(i, n):
                    go1, go2 = go[i], go[j]
                    try:
                        sim[i, j] = self.calc_ind_sim(go1, go2,
                                                      ontology, measure)
                    except:
                        print('Error in {}-{}'.format(go1, go2))
                        sim[j, i] = sim[i, j]
            sim = pd.DataFrame(sim, index=go, columns=go)
        else:
            sim = R.r['mgoSim'](R.StrVector(go), R.StrVector(go),
                                semData=self.ontologies[ontology],
                                measure=R.StrVector([measure]))
        return sim

    def calc_sets_sim(self, go1, go2, ontology='BP', measure="Wang"):
        self._check_args(measure, ontology)
        sim = R.r['mgoSim'](R.StrVector(go1), R.StrVector(go2),
                            semData=self.ontologies[ontology],
                            measure=R.StrVector([measure]),
                            combine=R.StrVector(["BMA"]))
        return sim

    def calc_gene_sets_sim(self, genes1, genes2, ontology='BP', measure="Wang"):
        self._check_args(measure, ontology)
        sim = R.r['clusterSim'](R.StrVector(genes1), R.StrVector(genes2),
                                semData=self.ontologies[ontology],
                                measure=R.StrVector([measure]),
                                combine=R.StrVector(["BMA"]))
        return sim



def load_motif_data(base_dir, prefix):
    fpath = join(base_dir, 'Neonatal.enrichment', 'matrices',
                 '{}.counts'.format(prefix))
    m = pd.read_csv(fpath, index_col=0, sep='\t') > 0
    return(m)


def fit_elasticnet(contrasts, groups, alpha, l1_ratio, n_iter=500):
    grid = {'l1_ratio': l1_ratio, 'alpha': alpha}
    best_params = {}
    estimates = {}

    for contrast in contrasts:
        X = load_motif_data(base_dir, prefix).astype(int)
        fpath = join(base_dir, '{}_grouping.tab'.format(contrast))
        df_groups = pd.read_csv(fpath, index_col=0, sep='\t')

        for group in groups:
            label = '{}-{}'.format(contrast, group)
            Y = (df_groups.ix[X.index , 0] == group).astype(int)

            if isiterable(alpha) or isiterable(l1_ratio):
                net = SGDClassifier(penalty='elasticnet', n_iter=n_iter)
                cv = GridSearchCV(estimator=net, param_grid=grid, cv=10)
                cv.fit(X, Y)
                best_params[label] = cv.best_params_
                model = cv.best_estimator_
            else:
                model = SGDClassifier(penalty='elasticnet', alpha=alpha,
                                      l1_ratio=l1_ratio, n_iter=n_iter)
                best_params[label] = {'alpha': alpha, 'l1_ratio': l1_ratio}

            model.fit(X, Y)
            coeffs = model.coef_.flatten()
            print(best_params[label], contrast, group, coeffs.shape)
            estimates[label] = dict(zip(X.columns, coeffs))

    estimates = pd.DataFrame.from_dict(estimates, orient='index')
    best_params = pd.DataFrame.from_dict(best_params, orient='index')
    return(estimates, best_params)


def plot_param_estimates(estimates, best_params, fpath):
    fig, subplots = init_fig(int(estimates.shape[0] / 2), 2,
                             colsize=7, rowsize=3)
    subplots = subplots.flatten()
    xs = np.arange(estimates.shape[1])

    for axes, label in zip(subplots, estimates.index):
        coeffs = estimates.loc[label, :]
        axes.scatter(xs, coeffs)
        axes.set_xlabel('Params')
        axes.set_ylabel('Estimates')
        title = [label] + ['{}={}'.format(key, value)
                           for key, value in best_params.loc[label].to_dict().items()]
        axes.set_title(' '.join(title))
        axes.set_xlim((0, estimates.shape[1]))
        axes.set_ylim((coeffs.min(), coeffs.max()))

    sns.despine()
    savefig(fig, fpath)


def run_elasticnet():
    base_dir = '/home/cmarti/microexons_analysis/mouse/metanalysis'
    contrasts = ['Neonatal', 'Adult', 'TAC', 'MI']
    prefix = 'clip_encode'
    groups = ['Included', 'Skipped']
    l1_ratio = [0, 0.1, 0.5, 0.75, 1]
    alpha = [0.0001, 0.001, 0.005, 0.01, 0.05, 0.1]
#     l1_ratio = [0, 0.1, 0.5]
#     alpha = [0.001, 0.005, 0.01]

    estimates, best_params = fit_elasticnet(contrasts, groups,
                                            alpha=0.001, l1_ratio=1)
    fpath = join(base_dir, 'plots', 'elastic_net.results2.png')
    plot_param_estimates(estimates, best_params, fpath)

#     estimates = pd.DataFrame.from_dict(estimates, orient='index')
#     fpath = join(base_dir, 'elastic_net.results.csv')
#     estimates.to_csv(fpath)

    estimates = pd.read_csv(fpath, index_col=0)
    filtered = estimates.loc[:, np.any(np.abs(estimates) > 0.1, axis=0)]
    print(filtered)
    best_params = pd.DataFrame.from_dict(best_params, orient='index')
    best_params.to_csv(join(base_dir, 'elastic_net.params.csv'))


def summary_plot(logors, logpvals, binding_df, zscores, diffs,
                 go_logors, go_logpvals, gosim, go2name, contrasts,
                 fpath, sfactor=20, h=6, w=14, vmax=3):
    sns.set(style="white", rc={"axes.facecolor": (0, 0, 0, 0)})
    fig = plt.figure(figsize=(w, h))
    gs = GridSpec(100, 300, wspace=0, hspace=0)

    # Dendrogram
    axes_dendro = fig.add_subplot(gs[13:84, :23], axisbg="white")
    labels = plot_dendrogram(binding_df, axes_dendro)

    logors = logors.loc[labels, :]
    logpvals = logpvals.loc[labels, :]
    go_logors = go_logors.loc[labels, :]
    go_logpvals = go_logpvals.loc[labels, :]

    # Dotplot
    axes_dotplot = fig.add_subplot(gs[10:87, 55:105])
    diffs = diffs.loc[labels[::-1], :]
    cmap_lim = (0, vmax)
    points = dotplot(logpvals, logors, axes_dotplot, cmap_lim=cmap_lim,
                     sfactor=sfactor, showyticklabels=True)
    cbar = colorbar(points, ax=axes_dotplot, shrink=0.4,
                    fraction=0.1, pad=-0.15, extend='both', aspect=15)
    cbar.set_label('$log_{2}(OddsRatio)$')
    _dotplot_legend(axes_dotplot, logpvals, sfactor=sfactor)

    # TODO: Fit random forest models to allow non linear effects and
    # perform RFECV to assess importance of binding sites
    # Add barplot or boxplot with CV losses for each feature (RBP-BS)
    # Similarly: add pairwise interactions and run Lasso: optimize alpha with CV
    # We may be able to penalize differently single and epistatic effects
    # (at least with Horseshoe prior)

    # Conservation barplots
    b_width = 4
    axes_x = 120
    diffs['index'] = diffs.index
    print(diffs)
    nplot = 0
    for contrast in contrasts:
        for group in ['Included', 'Skipped']:
            nplot += 1
            contrast_label = contrasts_labels.get(contrast, contrast)
            axes_barplot = fig.add_subplot(gs[13:84, axes_x:axes_x + b_width])
            axes_x += b_width + 1
            sns.barplot(x='{}-{}.d'.format(contrast_label, group),
                        y='index', data=diffs, n_boot=1, orient='h',
                        color='purple', ax=axes_barplot, linewidth=0,
                        alpha=1, capsize=0.2)

            for i, (mean, sem) in enumerate(zip(diffs['{}-{}.d'.format(contrast_label, group)],
                                                diffs['{}-{}.sem'.format(contrast_label, group)])):
                axes_barplot.plot([mean - sem, mean + sem], [i, i], color='black',
                                  linewidth=0.5)
                axes_barplot.plot([mean - sem, mean - sem], [i - 0.2, i + 0.2], color='black',
                                  linewidth=0.5)
                axes_barplot.plot([mean + sem, mean + sem], [i - 0.2, i + 0.2], color='black',
                                  linewidth=0.5)

            axes_barplot.set_xticks([0])
            axes_barplot.set_xticklabels(['{} {}'.format(contrast_label, group)],
                                         rotation=90)
            if nplot == 5:
                axes_barplot.set_xlabel(r'$\Delta phastCons$',
                                        fontsize=12)
            else:
                axes_barplot.set_xlabel('')

            sns.despine(ax=axes_barplot, left=True)
            ylims = axes_barplot.get_ylim()
            axes_barplot.plot((0, 0), ylims, linewidth=0.5, c='black')
            axes_barplot.set_ylim(ylims)
            axes_barplot.set_ylabel('')
            axes_barplot.set_yticklabels([])
            axes_barplot.set_xlim(-0.5, 0.5)

    # Expression heatmap
    zscores_axes = fig.add_subplot(gs[13:84, axes_x + 5:205])
    rbps = [label.split(' ')[0] for label in labels][::-1]
    total = pd.DataFrame({lab: {'total': value}
                              for lab, value in zscores.loc[np.unique(rbps), :].mean(0).to_dict().items()})
    zscores = pd.concat([zscores, total])[sorted_cols]
    zscores = zscores.loc[rbps, :]

    sns.heatmap(zscores,
                linewidths=0,
                linecolor='black', cbar=True,
                cbar_kws={'shrink': 0.4, 'aspect': 15,
                          'label': r'$log(Counts) - log(Counts)_{mean}$'},
                ax=zscores_axes,
                xticklabels=True, yticklabels=False)

    # Dendrogram for GO terms
    sim_m = gosim.loc[go_logpvals.columns, go_logpvals.columns]
    axes_dendro_go = fig.add_subplot(gs[:10, 226:287])
    Z = linkage(1 - sim_m, 'ward')
    dendro = dendrogram(Z, ax=axes_dendro_go, orientation='top',
                        color_threshold=0,
                        above_threshold_color='black', labels=sim_m.index,
                        no_labels=True)
    sns.despine(ax=axes_dendro_go, left=True, bottom=True)
    axes_dendro_go.set_yticklabels([])
    axes_dendro_go.set_ylabel('')
    xorder = dendro['ivl']

    # Dotplot for GO enrichment
    sfactor = 30
    axes_dotplot_go = fig.add_subplot(gs[10:87, 225:])
    points = dotplot(go_logpvals[xorder], go_logors[xorder],
                     axes_dotplot_go, cmap_lim=(0, 5),
                     sfactor=sfactor, showyticklabels=False)
    go_names = [go2name[x] for x in xorder]
    axes_dotplot_go.set_xticklabels(go_names, rotation=-45,
                                    fontsize=9, ha='left', va='top')
    cbar = colorbar(points, ax=axes_dotplot_go, shrink=0.4,
                    fraction=0.1, pad=-0.15, extend='both', aspect=15)
    cbar.set_label('$log_{2}(OddsRatio)$')
    _dotplot_legend(axes_dotplot_go, logpvals, sfactor=sfactor, x=1.1, y=0.1)

    fig.savefig(fpath, format=fpath.split('.')[-1], dpi=240)


def _dotplot_legend(axes, logpvals, sfactor, x=-1.2, y=-0.13):
    gll = plt.scatter([], [], s=2 * sfactor, marker='o', color='#555555')
    gl = plt.scatter([], [], s=4 * sfactor, marker='o', color='#555555')
    ga = plt.scatter([], [], s=6 * sfactor, marker='o', color='#555555')

    # Shrink current axis by 20%
    box = axes.get_position()
    axes.set_position([box.x0, box.y0, box.width * 0.8, box.height])

    # Put a legend to the right of the current axis

    leg = axes.legend((gll, gl, ga), ('2', '4', '6'), scatterpoints=1,
                    loc='center left', bbox_to_anchor=(x, y),
                    ncol=1, title='$-log_{10}(p-value)$',
                    fontsize=9, frameon=True, fancybox=True)
    xlims = axes.get_xlim()
#     axes.text(xlims[0] - 2.45, -1.7, r'$-log(p-value)$', rotation=90,
#               fontsize=10)


def get_rbp_label(label, rbps_names={}):
    region1_labels = {'Upstream': 'UP', 'Downstream': 'DW',
                      'Alternative': 'A'}
    region2_labels = {'intron': 'I', 'exon': 'E'}

    rbp, region = label.split('.', 1)
    region1, region2 = region.split('_', 1)
    region2, region3, lenght = region2.split('.')
    label = '{} {}-{}:{}'.format(rbps_names.get(rbp, rbp),
                                 region1_labels[region1],
                                 region2_labels[region2], region3.upper())
    return(label)


def get_data(base_dir, contrasts, db='clip_encode',
             region=None, sel_rbps=None, fdr=False, joint=False):
    logpvals = []
    logors = []
    colnames = []

    for prefix in ['Neonatal', 'Adult', 'TAC', 'MI']:
        contrast_label = contrasts.get(prefix, prefix)
        enrichment_dir = join(base_dir, '{}.enrichment'.format(prefix),
                              'enrichment')
        for group in ['Included', 'Skipped']:
            colnames.append('{} {}'.format(contrast_label, group))
            if joint:
                fpath = join(base_dir,
                             'joint.enrichment.{}.{}.csv'.format(prefix, group))
                enrichment = pd.read_csv(fpath, index_col=0).iloc[2:, :]
                enrichment.index = [x.rstrip('True') for x in enrichment.index]
                pvals = enrichment['Pr(>|z|)']
                logor = enrichment['Estimate']
            else:
                fpath = join(enrichment_dir,
                             '{}.{}_vs_No-change.tab'.format(db, group))
                enrichment = pd.read_csv(fpath, sep='\t', index_col=0)
                sel_rows = [not x.startswith('ENSG') for x in enrichment.index]
                enrichment = enrichment.loc[sel_rows, :]

                if region is not None:
                    sel_rows = [region in rowname for rowname in enrichment.index]
                    enrichment = enrichment.loc[sel_rows, :]
                if sel_rbps is not None:
                    sel_rows = [rowname.split('.')[0].upper() in sel_rbps
                                for rowname in enrichment.index]
                    enrichment = enrichment.loc[sel_rows, :]

                pvals = enrichment['pvalue']
                logor = np.log2(enrichment['OR'])
            if fdr:
                pvals = pd.DataFrame({'pvalue': padjust(pvals)},
                                     index=pvals.index)
            logpvals.append(-np.log10(pvals))
            logors.append(logor)

    logpvals = pd.concat(logpvals, axis=1)
    logpvals.columns = colnames
    logors = pd.concat(logors, axis=1)
    logors.columns = colnames
    return(logpvals, logors)


def rename_rows(df, cols=False):
    if cols:
        df.columns = [get_rbp_label(colname, RBPS_NAMES)
                      for colname in df.columns]
    else:
        df.index = [get_rbp_label(rowname, RBPS_NAMES) for rowname in df.index]


def _melt_data(logpvalues, logors):
    colnames = list(logpvalues.columns)
    rownames = list(logpvalues.index)
    logpvalues['RBP'] = rownames

    melted = pd.melt(logpvalues, id_vars=['RBP'])
    melted['logor'] = pd.melt(logors)['value']
    melted.columns = ['RBP', 'Contrast', 'logpval', 'logor']
    melted['y'] = [rownames.index(rbp) for rbp in melted['RBP']]
    melted['x'] = [colnames.index(contrast)  for contrast in melted['Contrast']]
    return(melted)


def filter_rows(logpvalues, logors, max_pval=0.01, min_or=1.5):
    minlogpval = -np.log(max_pval)
    min_logors = np.log2(min_or)
    sel_rows = np.any(np.logical_and(logpvalues > minlogpval,
                                     logors > min_logors), axis=1)
    logpvalues, logors = logpvalues.loc[sel_rows, :], logors .loc[sel_rows, :]
    return(logpvalues, logors)


def dotplot(logpvals, logors, axes, cmap_lim=(0, 3),
            sfactor=30, showyticklabels=True):
    df = _melt_data(logpvals, logors)
    cnumbers = np.array(df['logor'])
    cnumbers[cnumbers > 3] = cmap_lim[1]
    cnumbers[cnumbers < 0] = cmap_lim[0]
    points = axes.scatter(x=df['x'], y=df['y'],
                          s=sfactor * df['logpval'] + 1,
                          c=cnumbers, cmap='Reds',
                          edgecolor='black', linewidth=1,
                          vmax=cmap_lim[1], vmin=cmap_lim[0])
    axes.set_yticks(np.arange(logpvals.shape[0]))
    if showyticklabels:
        axes.set_yticklabels(logpvals.index, fontsize=10)
    else:
        axes.set_yticklabels([])
    axes.set_xticks(np.arange(logpvals.shape[1]))
    axes.set_xticklabels(logpvals.columns, rotation=90, fontsize=12)
    axes.set_yticklabels(axes.get_yticklabels(), fontsize=9)
    sns.despine(ax=axes, bottom=True, left=True)
    axes.set_xlabel('')
    axes.set_ylabel('')
    return(points)


def calc_MI_matrix(m):
    mi = np.zeros((m.shape[1], m.shape[1]))

    hs = [calc_H(m.iloc[:, i]) for i in range(m.shape[1])]

    for i in range(m.shape[1]):
        mi[i, i] = 1
        for j in range(i + 1, m.shape[1]):
            mi[i, j] = calc_MI(m.iloc[:, i], m.iloc[:, j],
                               h1=hs[i], h2=hs[j])
            mi[j, i] = mi[i, j]
    return(mi)


def calc_overlaps_matrix(binding_df):
    n = binding_df.shape[1]
    m = np.zeros((n, n))

    for i in range(n):
        m[i, i] = 1
        set1 = set(binding_df.loc[binding_df.iloc[:, i] == 1, :].index)
        for j in range(i + 1, n):
            set2 = set(binding_df.loc[binding_df.iloc[:, j] == 1, :].index)
            sim = len(set1.intersection(set2)) / float(len(set1.union(set2)))
            m[i, j] = sim
            m[j, i] = sim
    m = pd.DataFrame(m, index=binding_df.columns, columns=binding_df.columns)
    return(m)


def plot_dendrogram(binding_df, axes):
    mi = calc_overlaps_matrix(binding_df)
    labels = binding_df.columns
    Z = linkage(1 - mi, 'ward')
    dendro = dendrogram(Z, ax=axes, orientation='right', color_threshold=0,
                        above_threshold_color='black', labels=labels,
                        no_labels=True)
    sns.despine(ax=axes, left=True, bottom=True)
    axes.set_xticks(axes.get_xticks()[::2])
    axes.set_xlabel('')
    axes.set_xticklabels(axes.get_xticklabels(), fontsize=10)
    return(dendro['ivl'])


def load_motif_data(base_dir, prefix):
    fpath = join(base_dir, 'Neonatal.enrichment', 'matrices',
                 '{}.counts'.format(prefix))
    m = pd.read_csv(fpath, index_col=0, sep='\t') > 0
    sel_rows = [not x.startswith('ENSG') for x in m.index]
    m = m.loc[sel_rows, :]
    fpath = join(base_dir, 'Neonatal.enrichment', 'matrices',
                 '{}.data'.format(prefix))
    bs_data = pd.read_csv(fpath, sep='\t').set_index('event')
    return(m, bs_data)


def calc_cons_diffs(bs_data, contrasts, base_dir,
                    contrasts_labels):
    for contrast in contrasts:
        fpath = join(base_dir, '{}_grouping.tab'.format(contrast))
        groups = dict(tuple(line.strip().split()[:2]) for line in open(fpath))
        bs_data[contrast] = [groups.get(event_id, None) for event_id in bs_data.index]

    diffs = []
    for rbp, df in bs_data.groupby('rbp_region'):
        record = {'rbp': rbp}
        for contrast in contrasts:
            for group in ['Included', 'Skipped']:
                x = df.loc[df[contrast] == group, 'cons']
                y = df.loc[df[contrast] == 'No-change', 'cons']
                sem = np.sqrt(np.var(x) / x.shape[0] + np.var(y) / y.shape[0])
                label = '{}-{}'.format(contrasts_labels.get(contrast, contrast), group)
                record[label + '.sem'] = sem
                record[label + '.d'] = x.mean() - y.mean()
        diffs.append(record)
    diffs = pd.DataFrame(diffs).set_index('rbp')
    return(diffs)


if __name__ == '__main__':
    base_dir = '/home/cmarti/microexons_analysis/mouse/metanalysis'
    contrasts_labels = {'Adult': 'PD', 'Neonatal': 'ED'}
    contrasts = ['Neonatal', 'Adult', 'TAC', 'MI']
    fpath = '/home/cmarti/gene_sets/mouse/GO_data.tsv'
    go2name = dict(tuple(line.strip().split('\t')[:2][::-1]) for line in open(fpath)
                   if len(line.strip().split('\t')) >= 2)
    joint = True
    sfactor = 20
    h = 8

    for db in ['clip_merged', 'clip_encode', 'clip', 'attract']:

        # Load enrichment results
        logpvals, logors = get_data(base_dir, contrasts_labels, db=db,
                                    joint=joint)

        # Load sites data
        binding_df, bs_data = load_motif_data(base_dir, prefix=db)
        cons_diffs = calc_cons_diffs(bs_data, contrasts, base_dir,
                                     contrasts_labels)
        sel_rows = [not x.startswith('ENSG') for x in cons_diffs.index]
        cons_diffs = cons_diffs.loc[sel_rows, :].fillna(0)

        # Load expression data
        fpath = join(base_dir, 'expression.estimates.csv')
        logcounts = pd.read_csv(fpath, index_col=0).dropna()
        sorted_cols = ['Embryo', 'Neonatal', 'Adult', 'TAC', 'MI']
        zscores = logcounts.loc[:, sorted_cols].sub(logcounts.mean(1), axis=0)
        fpath = '/home/cmarti/gene_sets/mouse/gene_ids.gene_symbols.unique'
        gene_ids = pd.read_csv(fpath, index_col=0, sep='\t')
        common_ids = np.intersect1d(gene_ids.index, zscores.index)
        zscores = zscores.loc[common_ids, :]
        zscores.index = [RBPS_NAMES.get(x.upper(), x.upper())
                         for x in gene_ids.ix[common_ids, 0]]

        if not joint:
            if db == 'attract':
                logpvals, logors = filter_rows(logpvals, logors, max_pval=0.001,
                                               min_or=1.5)
                sfactor, h = 10, 14
            else:
                logpvals, logors = filter_rows(logpvals, logors, max_pval=0.01,
                                               min_or=1.2)
                sfactor, h = 15, 8

            with open(join(base_dir, '{}.sel_rbps.txt'.format(db)), 'w') as fhand:
                for rbp in logpvals.index:
                    fhand.write('{}\n'.format(rbp))

        binding_df = binding_df.loc[:, logpvals.index]
        if not joint:
            binding_df.to_csv(join(base_dir, '{}.bs.csv'.format(db)))

        # Load GO enrichment
        fpath = join(base_dir, 'GO_overlaps.csv')
        gosim = pd.read_csv(fpath, index_col=0)
        go_logpvals = -np.log10(pd.read_csv(join(base_dir, '{}.GO.pvalues.csv'.format(db)), index_col=0))
        go_logors = np.log2(pd.read_csv(join(base_dir, '{}.GO.oddsratio.csv'.format(db)), index_col=0))
        go_logpvals, go_logors = filter_rows(go_logpvals.transpose(), go_logors.transpose(), max_pval=0.1,
                                             min_or=1.2)
        go_logpvals, go_logors = go_logpvals.transpose(), go_logors.transpose()
        print(go_logpvals.shape, 'GO filtered')


        rename_rows(logors)
        rename_rows(logpvals)
        rename_rows(go_logors)
        rename_rows(go_logpvals)
        rename_rows(cons_diffs)
        rename_rows(binding_df, cols=True)

        if joint:
            fpath = join(base_dir, 'plots', '{}_joint_enrichment_plot.pdf'.format(db))
        else:
            fpath = join(base_dir, 'plots', '{}__plot.pdf'.format(db))
        summary_plot(logors, logpvals, binding_df, zscores, cons_diffs,
                     go_logors, go_logpvals, gosim, go2name,
                     contrasts, fpath, sfactor=sfactor, h=h, vmax=2)
        exit()



