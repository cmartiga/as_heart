#!/usr/bin/env python
from os.path import join

from matplotlib.gridspec import GridSpec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from scripts.settings import DATA_DIR, PLOTS_DIR
from scripts.plot_reg_interactions import init_plotting_params, load_grouping,\
    merge_cor_and_lasso_coefs, single_rbp_barplot, prep_matrices_heatmap,\
    make_heatmap
from scripts.plot_utils import add_panel_label


if __name__ == '__main__':
    init_plotting_params()
    contrasts = ['PD', 'TAC']
    n_contrasts = len(contrasts)

    # file paths
    lasso_fpath = join(DATA_DIR, 'clip_encode_lasso_pw.csv')
    cv_fpath = join(DATA_DIR, 'clip_encode_lasso_pw.cv_scores.csv')
    bs_fpath = join(DATA_DIR, 'clip_merged.bs.csv')
    rhos_fpath = join(DATA_DIR, 'rbps_condition_correlations.csv')

    # Load data
    bs = pd.read_csv(bs_fpath, index_col=0)
    rhos = pd.read_csv(rhos_fpath)    
    data = pd.read_csv(lasso_fpath)
    df = pd.melt(data, id_vars=['Type', 'rbp'])
    df['Contrast'] = [x.split()[0] for x in df['variable']]
    df['Change'] = [x.split()[1] for x in df['variable']]
    df['effect'] = df['value'] != 0
    groupings = load_grouping(contrasts)
    data = merge_cor_and_lasso_coefs(data, bs, rhos, groupings, contrasts)

    # Make figure
    fig = plt.figure(figsize=(11, 5))
    gs = GridSpec(65, 200, wspace=1, hspace=1)
    
    # Create matrix with interactions
    for (contrast, xpos, panel_label) in [(contrasts[0], 0, 'A'),
                                          (contrasts[-1], 120, 'B')]:
        c1, c2, ind_effects, m1, m2 = prep_matrices_heatmap(data, contrast)

        # Barplots for single effects
        axes = fig.add_subplot(gs[:10, xpos: xpos + 70])
        single_rbp_barplot(ind_effects, c1, axes, orient='vertical')

        axes = fig.add_subplot(gs[10:60, xpos + 70: xpos + 80])
        single_rbp_barplot(ind_effects, c2, axes, orient='horizontal')

        # Heatmap with interaction effects
        if contrast == contrasts[0]:
            axes = fig.add_subplot(gs[10:60, :70])
            cbar1ax = fig.add_subplot(gs[15:30, 85:88])
            cbar2ax = fig.add_subplot(gs[45:60, 85:88])
            make_heatmap(m1, m2, axes, cbar1ax, cbar2ax)
        else:
            axes = fig.add_subplot(gs[10:60, xpos: xpos + 70])
            cbar1ax. cbar2ax = None, None
            make_heatmap(m1, m2, axes, cbar1ax, cbar2ax)
        add_panel_label(axes, label=panel_label, xfactor=0.2, yfactor=0.2)

    # Save plot
    fpath = join(PLOTS_DIR, 'reg_interactions_supp.pdf')
    fig.savefig(fpath, format='pdf', dpi=240)
    
    fpath = join(PLOTS_DIR, 'reg_interactions_supp.png')
    fig.savefig(fpath, format='png', dpi=240)
