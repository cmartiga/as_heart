#!/usr/bin/env python
from os.path import join

from matplotlib.gridspec import GridSpec
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import statsmodels.api as sm
import statsmodels.formula.api as smf

from AS_heart.settings import DATA_DIR, PLOTS_DIR, CONTRASTS
from AS_heart.plot_utils import create_patches_legend


def get_rbp_label(label):

    rbps_names = {'NSR100': 'SRRM4', 'QKI': 'QK'}
    region1_labels = {'Upstream': 'UP', 'Downstream': 'DW',
                      'Alternative': 'A'}
    region2_labels = {'intron': 'I', 'exon': 'E'}

    rbp, region = label.split('.', 1)
    region1, region2 = region.split('_', 1)
    region2, region3, _ = region2.split('.')
    label = '{} {}-{}:{}'.format(rbps_names.get(rbp, rbp),
                                 region1_labels[region1],
                                 region2_labels[region2], region3.upper())
    return(label)


def init_plotting_params():
    sns.set_style('white')
    mpl.rcParams['xtick.labelsize'] = 8
    mpl.rcParams['ytick.labelsize'] = 8
#     mpl.rcParams['axes.facecolor'] = (0, 0, 0, 0)


def plot_regularization_auroc(cv_fit, contrasts, gs):
    ysize = 7
    ystart = 0
    for contrast in contrasts:
        axes = fig.add_subplot(gs[ystart:ystart + ysize, :38])
        ystart = ystart + ysize + 1
        for group, color in [('Included', 'darkred'), ('Skipped', 'blue')]:
            label = '{} {}'.format(contrast, group)
            cv_df = cv_fit.loc[cv_fit['label'] == label]
            x = [np.log10(float(x)) for x in cv_df['C']]
            y = cv_df['auroc']
            axes.plot(x, y, c=color, label=group)
        axes.set_xlabel('')
        axes.set_ylabel('')
        sns.despine(ax=axes)
        if contrast != contrasts[-1]:
            axes.set_xticklabels([])
        axes.set_yticklabels([])
        ylims = axes.get_ylim()

        if ystart == (ysize + 1):
            legend_loc = (0, 1.25)
            axes.legend(loc=legend_loc, fontsize=7.5, ncol=2,
                        frameon=True, fancybox=True)
        if contrast == contrasts[1]:
            axes.set_ylabel('Cross-validation AUROC')

        axes.text(-2.75, ylims[1] - 0.2 * (ylims[1] - ylims[0]), contrast, fontsize=8)
    axes.set_xlabel(r'$log_{10}(C_{regularization})$')


def plot_prop_nonzero_bar(df, contrasts, hue_order, axes):
    sns.barplot(x='Contrast', y='effect', hue='Type', n_boot=1,
                data=df, ax=axes, linewidth=1.5, edgecolor='black', ci=None,
                order=contrasts, hue_order=hue_order)
    axes.set_xlabel('Contrast')
    axes.set_ylabel('Proportion of non-zero effects')
    axes.set_ylim((0, 0.5))
    axes.legend(loc=1)
    sns.despine(ax=axes)


def plot_effect_sizes_boxplot(df, axes):
    sns.boxplot(x='Contrast', y='value', hue='Type', showfliers=False,
                data=df.loc[df['effect'], :], ax=axes,
                order=CONTRASTS, hue_order=hue_order)
    sns.stripplot(x='Contrast', y='value', hue='Type', 
                data=df.loc[df['effect'], :], ax=axes, size=2.5, dodge=True,
                order=CONTRASTS, hue_order=hue_order, alpha=0.5,
                jitter=0.2, edgecolor='black', linewidth=0.5)
    
    axes.set_xlabel('Contrast')
    xlims = axes.get_xlim()
    axes.plot(xlims, (0, 0), lw=1, color='white', linestyle='--')
    axes.set_ylabel('Non-zero effect size')
    axes.legend_.set_visible(False)
    sns.despine(ax=axes)


def plot_cum_effect_size_bar(df, axes, contrasts, hue_order):
    df['value'] = np.abs(df['value'])
    cumeffects = df.groupby(by=['Contrast', 'Type'])['value'].sum().reset_index()
    sns.barplot(x='Contrast', y='value', hue='Type', n_boot=1,
                data=cumeffects, ax=axes, linewidth=1.5, edgecolor='black', ci=None,
                order=contrasts, hue_order=hue_order)
    axes.set_xlabel('Contrast')
    axes.set_ylabel('Cumulative absolute effect size')
    axes.legend_.set_visible(False)
    sns.despine(ax=axes)


def merge_cor_and_lasso_coefs(data, bs, rhos, groupings, contrasts):
    conditions_labels = {'Neonatal': 'ED', 'Adult': 'PD'}
    
#     bs_sum = bs.sum(1)
    rhos['condition'] = [conditions_labels.get(x, x) for x in rhos['condition']]
    sel_rhos = rhos.loc[rhos['Type'] == 'RBPs', :]
    sel_rhos.index = ['{}-{}:{}'.format(gene1, gene2, contrast)
                      for gene1, gene2, contrast in zip(sel_rhos['gene1'],
                                                        sel_rhos['gene2'],
                                                        sel_rhos['condition'])]
    sel_rhos = sel_rhos['z'].to_dict()
    
    colnames = []
    for contrast in contrasts:
        grouping = groupings[contrast]
        exons = grouping.index[grouping.iloc[:, 0] != 'No-change']
        exons = np.intersect1d(exons, bs.index)
        zs = []
        for rowname in data['rbp']:
            items = rowname.split('*')
            if len(items) != 2:
                z = np.nan
            else:
                rbps = [item.split('.')[0] for item in items]

                rowname = '{}-{}:{}'.format(rbps[0], rbps[1], contrast)
                z = sel_rhos.get(rowname, np.nan)
                if np.isnan(z):
                    rowname = '{}-{}:{}'.format(rbps[1], rbps[0], contrast)
                    z = sel_rhos.get(rowname, np.nan)
            zs.append(z)
        colname = 'z_rho_{}'.format(contrast)
        data[colname] = zs
        colnames.append(colname)
        cols = ['{} Included'.format(contrast), '{} Skipped'.format(contrast)]
        data[contrast] = np.any(data[cols] != 0, axis=1)
    return(data)


def load_grouping():
    groupings = {}
    for contrast in CONTRASTS:
        fpath = join(DATA_DIR, '{}_grouping.tab'.format(contrast))
        groupings[contrast] = pd.read_csv(fpath, sep='\t', index_col=0)
    return(groupings)


def prep_matrices_heatmap(data, contrast):
    c1, c2 = '{} Included'.format(contrast), '{} Skipped'.format(contrast)
    ind_effects = data.loc[data['Type'] == 'Single', :]
    sorted_idx = np.argsort(np.abs(ind_effects[[c1, c2]].sum(1)))
    ind_effects = ind_effects.iloc[sorted_idx, :]

    rbps = list(ind_effects['rbp'])
    n = len(rbps)
    rbps_names = [get_rbp_label(label) for label in rbps]
    ind_effects.index = rbps_names

    m = np.full((n, n), fill_value=np.nan)
    pairs = dict(zip(data['rbp'], data[c1]))

    m2 = np.full((n, n), fill_value=np.nan)
    pairs2 = dict(zip(data['rbp'], data[c2]))

    for i, rbp1 in enumerate(rbps):
        for j in range(i + 1, n):
            rbp2 = rbps[j]
            try:
                m[i, j] = pairs['{}*{}'.format(rbp1, rbp2)]
                m2[j, i] = pairs2['{}*{}'.format(rbp1, rbp2)]
            except KeyError:
                m[i, j] = pairs['{}*{}'.format(rbp2, rbp1)]
                m2[j, i] = pairs2['{}*{}'.format(rbp2, rbp1)]
    m1 = pd.DataFrame(m, index=rbps_names, columns=rbps_names)
    m2 = pd.DataFrame(m2, index=rbps_names, columns=rbps_names)
    return(c1, c2, ind_effects, m1, m2)


def single_rbp_barplot(ind_effects, column, axes, orient='vertical'):
    if orient == 'vertical':
        sns.barplot(x='rbp', y=column, data=ind_effects, color='darkred',
                    n_boot=0, ci=False, ax=axes, orient=orient)
    else:
        sns.barplot(x=column, y='rbp', data=ind_effects, color='darkred',
                    n_boot=0, ci=False, ax=axes, orient=orient)
    
    sns.despine(ax=axes, bottom= orient == 'vertical',
                left=orient == 'horizontal')
    xlims = axes.get_xlim()
    ylims = axes.get_ylim()
    if orient == 'vertical':
        axes.plot(xlims, (0, 0), c='black', linewidth=0.5)
        axes.set_ylabel('Effect size', fontsize=6)
        axes.set_xlim(xlims)
    else:
        axes.plot((0, 0), ylims, c='black', linewidth=0.5)
        axes.set_ylim(ylims)
        axes.set_xlabel('Effect size', fontsize=6)
    
    axes.set_xticklabels([])
    axes.set_yticklabels([])


def make_heatmap(m1, m2, axes, cbar1ax, cbar2ax):
    sns.heatmap(m1, vmin=0, vmax=0.3, cmap='Reds', linewidths=0.1, cbar=True,
                ax=axes, xticklabels=False, yticklabels=False,
                cbar_ax=cbar1ax)
    sns.heatmap(m2, vmin=0, vmax=0.3, cmap='Blues', linewidths=0.1, cbar=True,
                ax=axes, xticklabels=True, yticklabels=True,
                cbar_ax=cbar2ax)
    axes.set_yticklabels(axes.get_yticklabels(), fontsize=4)
    axes.set_xticklabels(axes.get_xticklabels(), fontsize=4)
    if cbar1ax is not None:
        cbar1ax.set_ylabel('Included - Effect size', fontsize=10)
    if cbar2ax is not None:
        cbar2ax.set_ylabel('Skipped - Effect size', fontsize=10)


def joyplot_rhos(rhos, fig, ysize=8, ystart=112, xstart=30, xend=80,
                 conditions=['ED', 'PD', 'TAC', 'MI', 'PPI', 'Random']):
    
    xlims = rhos['z'].min(), rhos['z'].max()
    for condition in conditions:
        axes = fig.add_subplot(gs[ystart:ystart + ysize, xstart:xend])
        ystart = ystart + ysize - 2
        color = 'purple'
        if condition == 'PPI':
            z = rhos.loc[rhos['Type'] == 'PPI', 'z'].dropna()
            color = 'darkred'
        elif condition == 'Random':
            z = rhos.loc[rhos['Type'] == 'Random', 'z'].dropna()
            color = 'darkred'
        else:
            sel_rows = np.logical_and(rhos['condition'] == condition,
                                      rhos['Type'] == 'RBPs')
            z = rhos.loc[sel_rows, 'z'].dropna()

        sns.kdeplot(z, clip_on=False, shade=True, alpha=1, lw=1.5,
                    bw=.2, color=color)
        sns.kdeplot(z, clip_on=False, color='w', alpha=1, lw=2,
                    bw=.2)
        axes.text(xlims[0], 0.1, condition, fontweight="bold",
                  ha="left")
        axes.set_xlabel('')
        axes.set_yticklabels([])
        axes.legend_.set_visible(False)
        axes.set_xticklabels([])
        axes.set_xlim(xlims)
        q1, q2, q3 = np.percentile(z, [25, 50, 75])

        ylims = axes.get_ylim()
        axes.plot((q2, q2), ylims, color='white', lw=1, clip_on=False)
        axes.plot((q1, q1), ylims, color='white', lw=1, clip_on=False, linestyle='--')
        axes.plot((q3, q3), ylims, color='white', lw=1, clip_on=False, linestyle='--')
        axes.plot((0, 0), ylims, color='black', lw=1, clip_on=False, linestyle='-')

        axes.plot(xlims, (0, 0), lw=2, color='black', clip_on=False)
        axes.set_ylim(ylims)
        if condition == 'MI':
            axes.set_ylabel('Density')
        sns.despine(ax=axes, left=True, bottom=True)

    axes.set_xlabel(r'$z-score\ Pearson\ \rho$')
    axes.set_xticks([-1.5, 0, 1.5])
    axes.set_xticklabels(['-1.5', '0', '1.5'])


def plot_rho_vs_interaction(rho_by_contrast, axes):
    palette = {'Interaction': 'darkorange', 'No interaction': 'purple'}
    sns.boxplot(x='contrast', y='z', hue='interaction', data=rho_by_contrast,
                ax=axes, showfliers=False, palette=palette)
    sns.stripplot(x='contrast', y='z', hue='interaction', data=rho_by_contrast,
                ax=axes, alpha=0.2, jitter=0.2, edgecolor='black', linewidth=0.5,
                dodge=True, size=2.5, palette=palette)
    
    axes.set_xlabel('Contrast')
    ylims = list(axes.get_ylim())
    ylims[1] = ylims[1] + 1.5
    xlims = axes.get_xlim()
    axes.set_ylim(ylims)
    create_patches_legend(axes, palette, fontsize=9)
    axes.plot(xlims, (0, 0), lw=1, color='white', linestyle='--')
    axes.set_ylabel(r'$z-score\ Pearson\ \rho$')
    sns.despine(ax=axes)

if __name__ == '__main__':
    init_plotting_params()
    n_contrasts = len(CONTRASTS)
    hue_order = ['Single', 'Interaction']

    # file paths
    lasso_fpath = join(DATA_DIR, 'clip_encode_lasso_pw.csv')
    cv_fpath = join(DATA_DIR, 'clip_encode_lasso_pw.cv_scores.csv')
    bs_fpath = join(DATA_DIR, 'clip_merged.bs.csv')
    rhos_fpath = join(DATA_DIR, 'rbps_condition_correlations.csv')

    # Load data
    bs = pd.read_csv(bs_fpath, index_col=0)
    rhos = pd.read_csv(rhos_fpath)    
    data = pd.read_csv(lasso_fpath)
    df = pd.melt(data, id_vars=['Type', 'rbp'])
    df['Contrast'] = [x.split()[0] for x in df['variable']]
    df['Change'] = [x.split()[1] for x in df['variable']]
    df['effect'] = df['value'] != 0
    cv_fit = pd.read_csv(cv_fpath, index_col=0)
    cv_fit = pd.melt(cv_fit, id_vars=['label'])
    cv_fit.columns = ['label', 'C', 'auroc']
    groupings = load_grouping()
    data = merge_cor_and_lasso_coefs(data, bs, rhos, groupings, CONTRASTS)
    rho_by_contrast = []
    for contrast in CONTRASTS:
        for z, interaction in zip(data['z_rho_{}'.format(contrast)], data[contrast]):
            rho_by_contrast.append({'contrast': contrast, 'z': z,
                                    'interaction': 'Interaction' if interaction else 'No interaction', })
    rho_by_contrast = pd.DataFrame(rho_by_contrast)
    fpath = join(DATA_DIR, 'rbps_rho_by_interaction.csv')
    rho_by_contrast.to_csv(fpath)

    # Make figure
    fig = plt.figure(figsize=(11, 9.5))
    gs = GridSpec(150, 200, wspace=1, hspace=1)
    
    plot_regularization_auroc(cv_fit, CONTRASTS, gs)
    
    axes = fig.add_subplot(gs[:30, 57:97])
    plot_prop_nonzero_bar(df, CONTRASTS, hue_order, axes)
    
    axes = fig.add_subplot(gs[:30, 115:152])  
    plot_effect_sizes_boxplot(df, axes)  

    axes = fig.add_subplot(gs[:30, 165:])
    plot_cum_effect_size_bar(df, axes, CONTRASTS, hue_order)
    
    # Create matrix with interactions
    for (contrast, xpos, panel_label) in [(CONTRASTS[0], 0, 'E'),
                                          (CONTRASTS[-1], 120, 'F')]:
        c1, c2, ind_effects, m1, m2 = prep_matrices_heatmap(data, contrast)

        # Barplots for single effects
        axes = fig.add_subplot(gs[40:50, xpos: xpos + 70])
        single_rbp_barplot(ind_effects, c1, axes, orient='vertical')

        axes = fig.add_subplot(gs[50:100, xpos + 70: xpos + 80])
        single_rbp_barplot(ind_effects, c2, axes, orient='horizontal')

        # Heatmap with interaction effects
        if contrast == CONTRASTS[0]:
            axes = fig.add_subplot(gs[50:100, :70])
            cbar1ax = fig.add_subplot(gs[55:70, 85:88])
            cbar2ax = fig.add_subplot(gs[85:100, 85:88])
            make_heatmap(m1, m2, axes, cbar1ax, cbar2ax)
        else:
            axes = fig.add_subplot(gs[50:100, xpos: xpos + 70])
            cbar1ax. cbar2ax = None, None
            make_heatmap(m1, m2, axes, cbar1ax, cbar2ax)

    axes = fig.add_subplot(gs[115:, 110:160])
    plot_rho_vs_interaction(rho_by_contrast, axes)

    mpl.rcParams['axes.facecolor'] = (0, 0, 0, 0)
    joyplot_rhos(rhos, fig, ysize=8, ystart=112, xstart=30, xend=80,
                 conditions=['ED', 'PD', 'TAC', 'MI', 'PPI', 'Random'])

    # Save plot
    fpath = join(PLOTS_DIR, 'reg_interactions.pdf')
    fig.savefig(fpath, format='pdf', dpi=240)
    
    fpath = join(PLOTS_DIR, 'reg_interactions.png')
    fig.savefig(fpath, format='png', dpi=240)
