#!/usr/bin/env python
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import seaborn as sns

from AS_heart.settings import UPREGULATED, DOWNREGULATED, INCLUDED, SKIPPED


# Define palettes
current_palette = sns.color_palette()
DEG_PALETTE = {UPREGULATED: 'darkblue', DOWNREGULATED: 'darkred',
               'No change': 'grey'}
DAS_PALETTE = {INCLUDED: 'purple', SKIPPED: 'orange',
               'No change': 'grey'}
ECHO_LABELS = {'Normalized_MASS': 'Normalized cardiac mass',
               'MV_A': 'Mitral valve A velocity (ms/s)',
               'MV_EoverA': 'Mitral valve E/A ratio'}
as_palette = {'No-change': 'purple',
              'Skipped': sns.color_palette()[2],
              'Included': sns.color_palette()[4]}
vidal_palette = {'No AS-change': 'purple', 'AS-change': 'paleturquoise',
                 'No GE-change': 'blueviolet', 'GE-change': 'darkorange',
                 'No AS': 'darkgrey', 'Complex': 'lime',
                 'AS-insensitive': 'grey', }


# Define plotting style
sns.set_style('white')
# mpl.rcParams['xtick.labelsize'] = 8
# mpl.rcParams['ytick.labelsize'] = 8
# mpl.rcParams['axes.labelsize'] = 10
# mpl.rcParams['axes.facecolor'] = (0, 0, 0, 0)
cpal = sns.color_palette('Set1')
GROUP_PAL = {'AAV9-Control': cpal[0], 'AAV9-PTBP1': cpal[3],
             'Sham': 'peachpuff', 'TAC': 'tomato',
             'Control': 'peachpuff', 'Remote': 'salmon',
             'Border': 'darkorange', 'Infarct': 'red'}


# Functions
def init_fig(nrow=1, ncol=1, figsize=None, style='white',
             colsize=3, rowsize=3):
    sns.set_style(style)
    if figsize is None:
        figsize = (colsize * ncol, rowsize * nrow)
    fig, axes = plt.subplots(nrow, ncol, figsize=figsize)
    return(fig, axes)


def savefig(fig, fpath):
    fig.tight_layout()
    fig.savefig(fpath, format=fpath.split('.')[-1], dpi=360)
    plt.close()


def create_patches_legend(axes, colors_dict, loc=1, ncol=1, **kwargs):
    axes.legend(handles=[mpatches.Patch(color=color, label=label)
                         for label, color in sorted(colors_dict.items())],
                loc=loc, ncol=ncol, **kwargs)


def arrange_plot(axes, xlims=None, ylims=None, xlabel=None, ylabel=None,
                 showlegend=False, legend_loc=None, hline=None, vline=None,
                 rotate_xlabels=False, cols_legend=1, rotation=90,
                 legend_frame=False, title=None, ticklabels_size=None,
                 yticklines=False):
    if xlims is not None:
        axes.set_xlim(xlims)
    if ylims is not None:
        axes.set_ylim(ylims)
    if title is not None:
        axes.set_title(title)

    if xlabel is not None:
        axes.set_xlabel(xlabel)
    if ylabel is not None:
        axes.set_ylabel(ylabel)

    if showlegend:
        axes.legend(loc=legend_loc, ncol=cols_legend,
                    frameon=legend_frame, fancybox=legend_frame)
    elif axes.legend_ is not None:
        axes.legend_.set_visible(False)

    if hline is not None:
        xlims = axes.get_xlim()
        axes.plot(xlims, (hline, hline), linewidth=1, color='grey',
                  linestyle='--')
        axes.set_xlim(xlims)
    
    if vline is not None:
        ylims = axes.get_ylim()
        axes.plot((vline, vline), ylims, linewidth=1, color='grey',
                  linestyle='--')
        axes.set_ylim(ylims)
    
    if rotate_xlabels:
        axes.set_xticklabels(axes.get_xticklabels(), rotation=rotation)
    if ticklabels_size is not None:
        for tick in axes.xaxis.get_major_ticks():
            tick.label.set_fontsize(ticklabels_size)
        for tick in axes.yaxis.get_major_ticks():
            tick.label.set_fontsize(ticklabels_size)
    if yticklines:
        xlims = axes.get_xlim()
        for y in axes.get_yticks():
            axes.plot(xlims, (y, y), lw=0.4, alpha=0.2, c='grey')


def add_panel_label(axes, label, fontsize=20, yfactor=0.03, xfactor=0.215):
    xlims, ylims = axes.get_xlim(), axes.get_ylim()
    x = xlims[0] - (xlims[1] - xlims[0]) * xfactor
    y = ylims[1] + (ylims[1] - ylims[0]) * yfactor
    axes.text(x, y, label, fontsize=fontsize)


def plot_significance(axes, common_genes, pvalues, pval1=0.05, pval2=0.2,
                      y=1):
    for i, gene in enumerate(common_genes):
        if pvalues[gene] < pval1:
            axes.text(i, y, '*', fontsize=16, ha='center')
            axes.plot((i - 0.3, i + 0.3), (y + 0.05, y + 0.05), c='black')
        elif pvalues[gene] < pval2:
            axes.text(i, y + 0.1, '.', fontsize=16, ha='center')
            axes.plot((i - 0.3, i + 0.3), (y + 0.05, y + 0.05), c='black')


def plot_boxplot(data, x, y, hue, palette, axes, xlabel=None, ylabel='',
                 pvalues=None, showlegend=True, panel_label='',
                 legend_loc=1, dots_size=5, ylims=None, hue_order=None,
                 xorder=None, show_yticklabels=True, dodge=True,
                 legend_cols=1):
    sns.boxplot(x=x, y=y, hue=hue, dodge=dodge, 
                data=data, showfliers=False, palette=palette,
                ax=axes, hue_order=hue_order, order=xorder)
    sns.stripplot(x=x, y=y, hue=hue,
                  data=data, dodge=dodge, palette=palette,
                  ax=axes, jitter=0.2, edgecolor='black', linewidth=1,
                  size=dots_size, hue_order=hue_order,
                  order=xorder)

    if xlabel is not None:
        axes.set_xlabel(xlabel)
    else:
        axes.set_xlabel('')
        axes.set_xticklabels([])

    axes.set_ylabel(ylabel)

    sns.despine(ax=axes)
    xlims = axes.get_xlim()
    if ylims is None:
        ylims = axes.get_ylim()
    else:
        axes.set_ylim(ylims)
    for ytick in axes.get_yticks():
        axes.plot(xlims, (ytick, ytick), lw=0.3, alpha=0.3, c='grey')
    if pvalues is not None:
        axes.text(xlims[1] - (xlims[1] - xlims[0]) * 0.25,
                  ylims[1] - (ylims[1] - ylims[0]) * 0.025,
                  'p = {:.3f}'.format(pvalues[y]), fontsize=9)

    axes.set_xlim(xlims)

    if showlegend:
#         add_panel_label(axes, label=panel_label, xfactor=0.4)
        create_patches_legend(axes, palette, loc=legend_loc, ncol=legend_cols,
                              fontsize=8, fancybox=True, frameon=True)
    elif hasattr(axes, 'legend_') and axes.legend_ is not None:
        axes.legend_.set_visible(False)
    if not show_yticklabels:
        axes.set_yticklabels([])
        