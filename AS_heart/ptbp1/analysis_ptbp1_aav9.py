#!/usr/bin/env python

from os.path import join

import pandas as pd
from scripts.settings import DATA_DIR
from scripts.utils import perform_statsitical_analysis
import statsmodels.formula.api as smf


def analyse_echos():
    # Load eco results
    data = pd.read_csv(join(DATA_DIR, 'echo_data.csv'))
    data.columns = [x.replace(' ', '_').replace('/', 'over')
                    for x in data.columns]
    data['batch'] = [str(x) for x in data['batch']]
    variables = data.columns[5:-1]

    # Analyze ECOs results
    stat_results, batch_adjusted = perform_statsitical_analysis(data, variables)
    
    # Save results
    stat_results.to_csv(join(DATA_DIR, 'echo.analysis.csv'))
    batch_adjusted.to_csv(join(DATA_DIR, 'echo_data.adjusted.csv'))


def analyse_qpcrs(experiment, batch_correction):
    # Load qPCRs results
    data = pd.read_csv(join(DATA_DIR, '{}_qpcrs.csv'.format(experiment)))
    variables = data.columns[3:]
    
    # Analyze qPCRs
    results = perform_statsitical_analysis(data, variables,
                                           batch_correction=batch_correction)
    stat_results, batch_adjusted = results
    
    # Save results
    fpath = join(DATA_DIR, '{}_qpcrs.analysis.csv'.format(experiment))
    stat_results.to_csv(fpath)
    fpath = join(DATA_DIR, '{}_qpcrs.adjusted.csv'.format(experiment))
    batch_adjusted.to_csv(fpath)


def analyse_fibrosis():
    # Load data
    data = pd.read_csv(join(DATA_DIR, 'histology.csv'))
    variables = data.columns[2:]
    
    # Analyze data
    stat_results = perform_statsitical_analysis(data, variables,
                                           batch_correction=False)[0]
    
    # Save results
    stat_results.to_csv(join(DATA_DIR, 'histology.analysis.csv'))


def analyse_infarct_ptbp1():
    # Load data
    data = pd.read_csv(join(DATA_DIR, 'MI.ptbp1_qpcr.csv'))
    
    # Analysis
    model = smf.glm(formula='ptbp1 ~ 1 + remote + border + infarct', data=data)
    results = model.fit()
    estimate, pvalue = results.params, results.pvalues
    stat_results = pd.DataFrame({'Parameter': 'PTBP1',
                                 'Control': [estimate[0]] * 3,
                                 'Infarct': estimate[0] + estimate[1:],
                                 'P-value': pvalue[1:]})

    # Save results
    stat_results.to_csv(join(DATA_DIR, 'MI.ptbp1_qpcr.analysis.csv'))


if __name__ == '__main__':
    analyse_infarct_ptbp1()
    analyse_qpcrs('TAC', batch_correction=False)
    analyse_echos()
    analyse_fibrosis()
    analyse_qpcrs('PTBP1', batch_correction=True)
