#!/usr/bin/env python
from os.path import join
from _collections import defaultdict

from matplotlib.gridspec import GridSpec

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from AS_heart.plot_utils import DAS_PALETTE, add_panel_label
from AS_heart.settings import (PLOTS_DIR, DEF_REGIONS, CLIP_DIR)
from AS_heart.utils import (add_das_categories, load_vast_splicing)


def load_profiles(splc_data, regions=DEF_REGIONS):
    profiles = defaultdict(dict)
    for region in regions:
        fpath = join(CLIP_DIR, 'ptbp1.profile.{}'.format(region))
        sites = pd.read_csv(fpath, sep='\t', index_col=0)
        
        for group, sel_ids in splc_data.groupby('Group')['event_id']:
            print(group, sel_ids.shape)
            profile = sites.loc[np.intersect1d(sel_ids, sites.index), :].mean(0)
            profiles[region][group] = profile
    return(profiles)


if __name__ == '__main__':
    splc_data = load_vast_splicing()
    add_das_categories(splc_data, dpsi_threshold=0.05, p_threshold=-1)
    profiles = load_profiles(splc_data, regions=DEF_REGIONS)
    region_lengths = [int(x.split('.')[-1]) for x in DEF_REGIONS]

    # Figure
    sns.set(style="white")
    fig = plt.figure(figsize=(16, 4))
    subplots = []
    start = 0
    xspace = 10
    gs = GridSpec(10, np.sum(region_lengths).astype(int) + xspace * len(DEF_REGIONS))
    
    add_panel = True
    for l, region in zip(region_lengths, DEF_REGIONS):
        axes = fig.add_subplot(gs[2:, start:start + l])
        x = np.arange(0, l) if region.split('.')[1] == 'l' else np.arange(-l, 0)
        for group, profile in profiles[region].items():
            
            axes.plot(x, profile * 100, c=DAS_PALETTE[group], lw=1.2,
                      label=group)
            
        if start == 0:
            sns.despine(ax=axes)
            axes.set_ylabel('% of bound exons')
            axes.legend(loc=(5, 1.1), ncol=3, frameon=True, fancybox=True)
        else:
            sns.despine(ax=axes, left=True)
            axes.set_yticklabels([])
        axes.set_xlabel('')
        start = start + l + xspace
        if add_panel:
            add_panel_label(axes, 'E', xfactor=2.25)
            add_panel = False

    # Save figure
    fname = 'profiles_ptbp1'
    fpath = join(PLOTS_DIR, '{}.png'.format(fname))
    fig.savefig(fpath, format='png', dpi=360)
