#!/usr/bin/env python
from os.path import join

import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import pandas as pd
import seaborn as sns

from scripts.settings import DATA_DIR, PLOTS_DIR, AS_CANDIDATES
from scripts.plot_utils import plot_boxplot, GROUP_PAL, add_panel_label,\
    init_fig, savefig


def figure_lrp4():
    psi = pd.read_csv(join(DATA_DIR, 'MmuEX0026886.vastdb.psi.csv'))
    heart_psi = pd.read_csv(join(DATA_DIR, 'MmuEX0026886.psi.csv'),
                            index_col=0)
    heart_psi = pd.melt(heart_psi)
    
    # Init Figures
    fig = plt.figure(figsize=(8, 10))
    gs = GridSpec(165, 100)
    
    # Empty to add browser
    axes = fig.add_subplot(gs[:45, :70])
    sns.despine(ax=axes, left=True, bottom=True)
    axes.set_xticklabels([])
    axes.set_yticklabels([])
    add_panel_label(axes, label='A', xfactor=0.15)
    
    # Dataset psi
    order = ['Embryo', 'Neonatal', 'Adult', 'AAV9-Control',
             'TAC', 'MI', 'AAV9-PTBP1']
    axes = fig.add_subplot(gs[:40, 70:])
    sns.barplot(x='variable', y='value', data=heart_psi, order=order,
                color='black', axes=axes, alpha=1)
    axes.set_xlabel('')
    axes.set_ylabel("Mouse LRP4 exon 37' $\hat\Psi$")
    axes.set_xticklabels(order, rotation=45, fontsize=8, ha='right')
    sns.despine(ax=axes)
    add_panel_label(axes, label='B', xfactor=0.25)
    
    # VASTDB mouse
    data = psi.loc[psi['Species'] == 'Mouse']
    tissues = data.sort_values('PSI')['Group']
    axes = fig.add_subplot(gs[55:100, :])
    sns.barplot(x='Group', y='PSI', data=data,
                color='black', axes=axes, order=tissues, alpha=1)
    axes.set_xlabel('')
    axes.set_ylabel("Mouse LRP4 exon 37' $\hat\Psi$")
    add_panel_label(axes, label='C', xfactor=0.12)
    axes.set_xticklabels([x.replace('_', ' ').capitalize() for x in tissues],
                         rotation=60, fontsize=6, ha='right')
    sns.despine(ax=axes)
    
    # VASTDB chicken
    data = psi.loc[psi['Species'] == 'Chicken']
    tissues = data.sort_values('PSI')['Group']
    axes = fig.add_subplot(gs[120:165, :])
    sns.barplot(x='Group', y='PSI', data=data,
                color='black', axes=axes, order=tissues, alpha=1)
    axes.set_xlabel('')
    axes.set_ylabel("Chicken ortholog LRP4 exon 37' $\hat\Psi$")
    add_panel_label(axes, label='D', xfactor=0.12)
    axes.set_xticklabels([x.replace('_', ' ').capitalize() for x in tissues],
                         rotation=60, fontsize=6, ha='right')
    sns.despine(ax=axes)
    
    # Save plot
    fpath = join(PLOTS_DIR, 'lrp4_psi.png')
    fig.savefig(fpath, format='png', dpi=240)


def figure_mouse(exon_id):
    data = pd.read_csv(join(DATA_DIR, '{}.vastdb.psi.csv'.format(exon_id)))
    
    # Init Figures
    fig, axes = init_fig(1, 1, colsize=8, rowsize=4)
    
    # VASTDB mouse
    tissues = data.sort_values('PSI')['Group']
    sns.barplot(x='Group', y='PSI', data=data,
                color='black', axes=axes, order=tissues, alpha=1)
    axes.set_xlabel('')
    axes.set_ylabel("$\hat\Psi$")
    axes.set_xticklabels([x.replace('_', ' ').capitalize() for x in tissues],
                         rotation=60, fontsize=6, ha='right')
    sns.despine(ax=axes)
    
    # Save plot
    fpath = join(PLOTS_DIR, '{}_psi.eps'.format(exon_id))
    savefig(fig, fpath)


if __name__ == '__main__':
#     figure_lrp4()
    for exon_id in ['MmuEX0026173', 'MmuEX0008863']:
        figure_mouse(exon_id)
    