#!/usr/bin/env python
from os.path import join

from matplotlib.gridspec import GridSpec

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scripts.plot_utils import DAS_PALETTE, add_panel_label
import seaborn as sns
from scripts.settings import (DAS_GROUPS, SETS_DBS, DPSI_THRESHOLD,
                              PLOTS_DIR, SETS_LABELS,
                              INCLUDED, SKIPPED, EVENT_TYPES_VAST, UPREGULATED,
    DOWNREGULATED)
from scripts.utils import (load_functional_enrichment,
                           add_das_categories, get_enrich_logfdr_matrix,
                           load_vast_splicing, add_event_types_vast)
from _collections import defaultdict


def calc_changes_freqs(data, col_by, group_col='Group'):
    ctable = pd.crosstab(index=data[col_by], columns=data[group_col],
                         margins=True).transpose()
    freqs = ctable / ctable.ix['All'] * 100
    freqs['Group'] = freqs.index
    freqs = pd.melt(freqs.drop('All'), id_vars=['Group'])
    return(freqs)


def get_region(name):
    flanks = {'r': 'dw', 'l': 'up'}
    items = name.split('.')
    region = items[1].split('_')
    region = (region[0][0] + region[1][0]).upper()
    region += '-{}'.format(flanks[items[2]])
    return(region)


if __name__ == '__main__':
    # Load data
    changes_groups =  [UPREGULATED, DOWNREGULATED,
                      'CE_' + INCLUDED, 'CE_' + SKIPPED]

    # Load enrichment results
    enrich_results = {}
    for db in SETS_DBS:
        enrich_res = load_functional_enrichment(db, changes_groups, 10)
        logfdrs = get_enrich_logfdr_matrix(enrich_res)
        max_values = logfdrs.max(axis=1)
        enrich_results[db] = logfdrs.iloc[np.argsort(max_values), :] #[-20:]

    # Figure
    sns.set(style="white")
    fig = plt.figure(figsize=(8, 20))
    gs = GridSpec(110, 60)

    # Plot functional enrichment analysis
    subplots = [fig.add_subplot(gs[60:, 47:]),
                fig.add_subplot(gs[:50, 47:])]
    for db, axes in zip(SETS_DBS[::-1], subplots):
        db_enrich = enrich_results[db][changes_groups]
        db_enrich.columns = [x.replace('_', ' ') for x in db_enrich.columns]
        sns.heatmap(data=db_enrich, cmap='Blues', linewidths=0,
                    linecolor='white', cbar=True, ax=axes,
                    xticklabels=True, yticklabels=True,
                    cbar_kws={'label': r'$-log_{10}(FDR)$'},
                    vmax=3)
        axes.set_xticklabels(axes.get_xticklabels(), rotation=90)
        axes.set_yticklabels(axes.get_yticklabels(), fontsize=9)
        axes.set_ylabel('')
        axes.set_xlabel('')
        axes.set_title(SETS_LABELS[db])
    axes.set_xticklabels([])

    add_panel_label(subplots[1], 'F', xfactor=4.5)
    add_panel_label(subplots[0], 'G', xfactor=4.50)
    

    # Save figure
    fpath = join(PLOTS_DIR, 'figure_functional_analysis.png')
    fig.savefig(fpath, format='png', dpi=100)
