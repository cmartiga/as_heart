#!/usr/bin/env python

from os.path import join

import pandas as pd
from scripts.settings import (DEG_GROUPS, FUNCTIONS_DIR, SETS_DBS, DEG,
                              INCLUDED, SKIPPED, EVENT_TYPES_VAST)
from scripts.utils import (load_expression, add_deg_categories, load_ids2symbols,
                           add_das_categories, get_splc_gene_groups,
                           LogTrack, load_gene_sets, enrichment_analysis,
                           load_vast_splicing, add_event_types_vast)


if __name__ == '__main__':
    log = LogTrack()
    log.write('Start AS functional analysis')

    # Load expression data
    ids2symbols = load_ids2symbols()
    deg = load_expression(ids2symbols)
    add_deg_categories(deg, qval_threshold=0.4, beta_threshold=0)

    # Load splicing data
    splc_data = load_vast_splicing()
    add_das_categories(splc_data, dpsi_threshold=0)
    add_event_types_vast(splc_data)
    splc_by_gene = get_splc_gene_groups(splc_data, by='Type',
                                        groups=[INCLUDED, SKIPPED]).dropna()
    splc_data.set_index('event_id', inplace=True)

    # Merge GE and AS changes groups
    merged = pd.concat([splc_by_gene, deg[DEG_GROUPS]], axis=1).dropna(0)
    splc_data['intercept'] = 1
    splc_by_gene['intercept'] = 1
    merged['intercept'] = 1
    msg = 'Loaded AS data for {} genes'.format(splc_by_gene.shape[0])
    log.write(msg)

    # Load gene sets databases
    dbs = {db: load_gene_sets(db, genes=merged.index) for db in SETS_DBS}

    # Perform enrichment
    for change_type in [INCLUDED, SKIPPED]:
        for event_type in EVENT_TYPES_VAST:
            group = '{}_{}'.format(event_type, change_type)
            log.write('Performing enrichment for {} genes'.format(group))

            # Perform enrichment for gene sets
            for db in SETS_DBS:
                log.write('\tGene sets: {}'.format(db))
                gene_sets = dbs[db]
                results = enrichment_analysis(gene_sets, merged, group)
                fname = '{}.ORA_{}.csv'.format(db, group)
                fpath = join(FUNCTIONS_DIR, fname)
                results.to_csv(fpath)

            # Perform enrichment for GE groups
            log.write('\tGene sets: {}'.format(DEG))
            results = enrichment_analysis(merged[DEG_GROUPS], merged, group)
            fname = '{}.ORA_{}.csv'.format(DEG, group)
            fpath = join(FUNCTIONS_DIR, fname)
            results.to_csv(fpath)

    log.finish()
