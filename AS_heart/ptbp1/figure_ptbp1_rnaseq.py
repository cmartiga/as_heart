#!/usr/bin/env python
from os.path import join
from matplotlib.gridspec import GridSpec

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scripts.plot_utils import DEG_PALETTE, add_panel_label, init_fig, savefig,\
    DAS_PALETTE
import seaborn as sns
from scripts.settings import (DEG_GROUPS, UPREGULATED, DEG_FPATH,
                              DOWNREGULATED, QVAL, BETA, QVAL_THRESHOLD,
                              BETA_THRESHOLD, PLOTS_DIR, DAS_FPATH, INCLUDED,
    SKIPPED, DPSI, DAS_GROUPS, DATA_DIR)
from scripts.utils import (add_deg_categories, add_das_categories,
    load_vast_splicing)
from scipy.stats.mstats_basic import pearsonr


def volcano_plot(deg, axes):
    # Plot unchanged genes in grey
    nosig = deg.loc[deg[UPREGULATED] + deg[DOWNREGULATED] == 0, :]
    axes.scatter(nosig[BETA], -np.log10(nosig[QVAL]), c='grey',
                 edgecolor='grey', s=1.3)

    # Plot with different colors up and downregulated genes
    for group in DEG_GROUPS:
        deg_group = deg.loc[deg[group] == 1, :]
        if deg_group.shape[0] == 0:
            continue
        xpos, ypos = deg_group[BETA], -np.log10(deg_group[QVAL])
        module = np.sqrt(xpos ** 2 + ypos ** 2)
        sizes = module * (10 / np.percentile(module, q=95))
        axes.scatter(xpos, ypos, c=DEG_PALETTE[group],
                     edgecolor=DEG_PALETTE[group], s=sizes)

    # Avoid showing very extreme values
    sig = deg.loc[deg[UPREGULATED] + deg[DOWNREGULATED] > 0, :]
    if sig.shape[0] > 0:
        xlims = 2 * np.percentile(sig[BETA], q=[1, 99])
        ylims = (0, 1.2 * np.percentile(-np.log10(sig[QVAL]), q=99))
    else:
        xlims = axes.get_xlim()
        ylims = axes.get_ylim()

    # Add auxiliary lines for selected thresholds
    axes.plot(xlims, (-np.log10(QVAL_THRESHOLD), -np.log10(QVAL_THRESHOLD)),
              linewidth=0.7, linestyle='--', c='blue', alpha=0.5)
    axes.plot((BETA_THRESHOLD, BETA_THRESHOLD), ylims,
              linewidth=0.7, linestyle='--', c='blue', alpha=0.5)
    axes.set_xlim(xlims)
    axes.set_ylim(ylims)

    # Set plot labels
    axes.set_xlabel(r'PTBP1-OE log$_{2}$(FC)', fontsize=14)
    axes.set_ylabel(r'-log$_{10}$(FDR)', fontsize=14)
    sns.despine(ax=axes)
    

def cor_expression(deg, deg2, axes, group2):
    sel_rows = np.intersect1d(deg.index, deg2.index)
    deg = deg.loc[sel_rows, :]
    deg['logfc_2'] = deg2.loc[sel_rows, 'logFC']
    axes.scatter(deg['logfc_2'], deg['logFC'], s=3, c='grey')
    axes.set_xlabel(group2 + r' $log_{2}(FC)$')
    axes.set_ylabel('PTBP1-OE $log_{2}(FC)$')
    sns.despine(ax=axes)
    
    xlims, ylims = axes.get_xlim(), axes.get_ylim()
    axes.plot(xlims, (0, 0), linestyle='--', lw=0.5, c='blue')
    axes.plot((0, 0), ylims, linestyle='--', lw=0.5, c='blue')
    axes.set_xlim(xlims)
    axes.set_ylim(ylims)

    rho = pearsonr(deg['logfc_2'], deg['logFC'])[0]
    axes.text(-2.5, 4, r'$\rho$ = {:.2f}'.format(rho))


def dAS_plot(das, axes):
    # Plot unchanged genes in grey
    nosig = das.loc[das[INCLUDED] + das[SKIPPED] == 0, :]
    axes.scatter(nosig['Control'], nosig['PTBP1'], c='grey',
                 edgecolor='grey', s=1.3, alpha=0.5)

    # Plot with different colors included and skipped exons
    for group in DAS_GROUPS:
        das_group = das.loc[das[group] == 1, :]
        if das_group.shape[0] == 0:
            continue
        xpos, ypos = das_group['Control'], das_group['PTBP1']
        axes.scatter(xpos, ypos, c=DAS_PALETTE[group], label=group,
                     edgecolor=DAS_PALETTE[group], s=3)

    xlims = (0, 1)
    ylims = (0, 1)

    # Add auxiliary lines for selected thresholds
    axes.plot(xlims, (0, 1),
              linewidth=0.7, linestyle='--', c='blue', alpha=0.5)
    axes.set_xlim(xlims)
    axes.set_ylim(ylims)

    # Set plot labels
    axes.set_xlabel(r'$\Psi_{Control}$', fontsize=14)
    axes.set_ylabel(r'$\Psi_{PTBP1}$', fontsize=14)
    axes.legend(loc=0, frameon=True, fancybox=True, fontsize=10)
    sns.despine(ax=axes)



def cor_splicing(das, das2, axes, group2):
    sel_rows = np.intersect1d(das.index, das2.index)
    das = das.loc[sel_rows, :]
    das['dpsi_2'] = das2.loc[sel_rows, 'dPSI']
    
    # Plot unchanged genes in grey
    nosig = das.loc[das[INCLUDED] + das[SKIPPED] == 0, :]
    axes.scatter(nosig['dpsi_2'], nosig[DPSI], c='grey',
                 edgecolor='grey', s=1.3, alpha=0.5)

    # Plot with different colors included and skipped exons
    for group in DAS_GROUPS:
        das_group = das.loc[das[group] == 1, :]
        if das_group.shape[0] == 0:
            continue
        xpos, ypos = das_group['dpsi_2'], das_group[DPSI]
        axes.scatter(xpos, ypos, c=DAS_PALETTE[group], label=group,
                     edgecolor=DAS_PALETTE[group], s=3)
    
    axes.set_xlabel(group2 + r' $\Delta\Psi$')
    axes.set_ylabel('PTBP1-OE $\Delta\Psi$')
    sns.despine(ax=axes)
    
    xlims, ylims = axes.get_xlim(), axes.get_ylim()
    axes.plot(xlims, (0, 0), linestyle='--', lw=0.5, c='blue')
    axes.plot((0, 0), ylims, linestyle='--', lw=0.5, c='blue')
    axes.set_xlim(xlims)
    axes.set_ylim(ylims)

    rho = pearsonr(das['dpsi_2'], das[DPSI])[0]
    axes.text(-0.5, 0.4, r'$\rho$ = {:.2f}'.format(rho))


def build_psi_matrix(das, das_mi, das_tac, kd):
    common_ids = np.intersect1d(np.intersect1d(das.index, das_mi.index),
                                das_tac.index)
    common_ids = np.intersect1d(common_ids, kd.index)
    data = das.reindex(common_ids)
    data['TAC'] = das_tac.loc[common_ids, 'TAC']
    data['Adult'] = das_tac.loc[common_ids, 'Adult']
    data['MI'] = das_mi.loc[common_ids, 'MI']
    data['shControl'] = kd['WT']
    data['shPTBP1/2'] = kd['shPTBP1_2']
    columns = ['Control', 'Adult', 'PTBP1', 'TAC', 'MI', 'shControl',
               'shPTBP1/2']
    sel_rows = np.logical_and(data['PTBP1'] - data['Control'] < -0.1,
                              data['TAC'] - data['Adult'] < -0.1)
    sel_rows = np.logical_and(sel_rows, data['MI'] - data['Adult'] < -0.1)
    data = data.loc[sel_rows, :]
    new_ids = ['{}-{}'.format(gene, exon)
               for gene, exon in zip(data['gene_name'], data.index)]
    data.index = new_ids
    data = data[columns]
    data.columns = columns = ['AAV9-Control', 'Adult', 'AAV9-PTBP1', 'TAC',
                              'MI', 'shControl', 'shPTBP1/2']
    return(data)
    

def psi_heatmap(psi):
    print(psi)
    fig = sns.clustermap(psi, cmap='Blues', dendrogram_ratio=(0.15, 0.05),
                         figsize=(5, 7),
                         cbar_kws={'label': r'Exon inclusion ratio $\Psi$'},
                         cbar_pos=(-0.15, .3, .03, .4))
    fpath = join(PLOTS_DIR, 'psi_clustermap.png')
    fig.savefig(fpath, format='png', dpi=360)


if __name__ == '__main__':
    fig_name = 'figure_rna_seq'

    # Load expression data
    deg = pd.read_csv(DEG_FPATH, sep='\t', decimal=',')
    add_deg_categories(deg, qval_threshold=QVAL_THRESHOLD,
                       beta_threshold=BETA_THRESHOLD)
    tac_deg = pd.read_csv(join(DATA_DIR, 'TAC.differential_expression.csv'),
                          index_col=0)
    mi_deg = pd.read_csv(join(DATA_DIR, 'MI.differential_expression.csv'),
                         index_col=0)
    
    # Load AS data
    das = load_vast_splicing()
    das[DPSI] = das['PTBP1'] - das['Control']
    add_das_categories(das, dpsi_threshold=0)
    das_tac = pd.read_csv(join(DATA_DIR, 'TAC.differential_AS.csv'), index_col=0)
    das_mi = pd.read_csv(join(DATA_DIR, 'MI.differential_AS.csv'), index_col=0)
    kd = pd.read_csv(join(DATA_DIR, 'ptbp1.kd.differential_AS.csv'), index_col=0)
    psi = build_psi_matrix(das, das_mi, das_tac, kd)
    psi.to_csv(join(DATA_DIR, 'ptbp1.psi.csv'))
    psi_heatmap(psi)
    
    # Figure
    sns.set(style="white")
    fig, subplots = init_fig(2, 3, rowsize=3.5, colsize=4)

    # DEG plots
    axes = subplots[0][0]
    volcano_plot(deg, axes)
    add_panel_label(axes, 'A', xfactor=0.25)
    
    axes = subplots[0][1]
    cor_expression(deg, tac_deg, axes, 'TAC')
    add_panel_label(axes, 'C', xfactor=0.25)
    
    axes = subplots[0][2]
    cor_expression(deg, mi_deg, axes, 'MI')
    add_panel_label(axes, 'E', xfactor=0.25)
    
    # Plot delta PSI
    axes = subplots[1][0]
    dAS_plot(das, axes)
    add_panel_label(axes, 'B', xfactor=0.25)
    
    axes = subplots[1][1]
    cor_splicing(das, das_tac, axes, 'TAC')
    add_panel_label(axes, 'D', xfactor=0.25)
    
    axes = subplots[1][2]
    cor_splicing(das, das_mi, axes, 'MI')
    add_panel_label(axes, 'F', xfactor=0.25)
    
    # save plot
    fpath = join(PLOTS_DIR, '{}.png'.format(fig_name))
    savefig(fig, fpath)
