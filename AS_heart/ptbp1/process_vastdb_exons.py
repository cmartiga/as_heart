#!/usr/bin/env python

from os.path import join
import pandas as pd
from scripts.settings import DATA_DIR

def extract_counts(data):
    counts = []
    for psi, qc_str in zip(data['Value'], data['QC']):
        inc, skp = qc_str.split('@')[1].split(',')
        inc, skp = float(inc), float(skp)
        total = inc + skp
        if total == 0:
            inc, skp = 0, 0
        else:
            skp = (total * (1 - psi)) / (psi + 1)
            inc = total  - skp
        counts.append({'total': total, 'inclusion': inc, 'skipping': skp})
    return(pd.DataFrame(counts, index=data.index).astype(int))

if __name__ == '__main__':
    # Lrp4 exon
    merged = []
    for sp in ['mouse', 'chicken']:
        fpath = join(DATA_DIR, 'MmuEX0026886.vastdb.{}.csv'.format(sp))
        data = pd.read_csv(fpath, sep='\t', index_col=0)
        data['Value'] = data['Value'] / 100
        data = data.join(extract_counts(data))
        data['sp'] = sp.capitalize()
        merged.append(data)
    merged = pd.concat(merged)
    merged.to_csv(join(DATA_DIR, 'MmuEX0026886.vastdb.tsv'), sep='\t')
    
    # Lass6 and Cad exons
    for exon_id in ['MmuEX0026173', 'MmuEX0008863']:
        fpath = join(DATA_DIR, '{}.vastdb.mouse.csv'.format(exon_id))
        data = pd.read_csv(fpath, sep='\t', index_col=0)
        data['Value'] = data['Value'] / 100
        data = data.join(extract_counts(data))
        data.to_csv(join(DATA_DIR, '{}.vastdb.tsv'.format(exon_id)), sep='\t')
