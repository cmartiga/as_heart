#!/usr/bin/env python
from os.path import join

import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import pandas as pd

from scripts.settings import (DATA_DIR, PLOTS_DIR,
                              HEART_FUNC_MARKERS, FIBROSIS_MARKERS)
from scripts.plot_utils import plot_boxplot, GROUP_PAL, add_panel_label


if __name__ == '__main__':
    qpcrs = pd.read_csv(join(DATA_DIR, 'PTBP1_qpcrs.adjusted.csv'))
    tac_qpcrs = pd.read_csv(join(DATA_DIR, 'TAC_qpcrs.adjusted.csv'))
    mi_qpcr = pd.read_csv(join(DATA_DIR, 'MI.ptbp1_qpcr.csv'))
    histology = pd.read_csv(join(DATA_DIR, 'histology.csv'))
    histology['perc'] = histology['prop_fibrosis'] * 100
    echos = pd.read_csv(join(DATA_DIR, 'echo_data.adjusted.csv'))
    levels = ['AAV9-Control', 'AAV9-PTBP1']
    pal = {k: GROUP_PAL[k] for k in levels}
    
    # Init Figures
    fig = plt.figure(figsize=(7, 7))
    gs = GridSpec(110, 100)
    
    # TAC Ptbp1 expression
    axes = fig.add_subplot(gs[:25, :20])
    levels = ['Sham', 'TAC']
    pal = {k: GROUP_PAL[k] for k in levels}
    plot_boxplot(tac_qpcrs, x='treatment', y='ptbp1',
                 hue='treatment', palette=pal, axes=axes,
                 xlabel='', ylabel='PTBP1 relative expression',
                 pvalues=None, showlegend=False,
                 legend_loc=(0, 1.02), dots_size=5, ylims=None,
                 hue_order=levels, xorder=levels, show_yticklabels=True,
                 dodge=False, legend_cols=2)
    add_panel_label(axes, label='A', xfactor=0.8)
    
    # MI Ptbp1 expression
    axes = fig.add_subplot(gs[:25, 35:])
    levels = ['Control', 'Remote', 'Border', 'Infarct']
    pal = {k: GROUP_PAL[k] for k in levels}
    plot_boxplot(mi_qpcr, x='Group', y='ptbp1',
                 hue='Group', palette=pal, axes=axes,
                 xlabel='', ylabel='PTBP1 relative expression',
                 pvalues=None, showlegend=False,
                 legend_loc=(0, 1.02), dots_size=5, ylims=None,
                 hue_order=levels, xorder=levels, show_yticklabels=True,
                 dodge=False, legend_cols=4)
    add_panel_label(axes, label='B', xfactor=0.19)
    
    # Ptbp1 expression
    levels = ['AAV9-Control', 'AAV9-PTBP1']
    pal = {k: GROUP_PAL[k] for k in levels}
    axes = fig.add_subplot(gs[40:65, :20])
    plot_boxplot(qpcrs, x='treatment', y='ptbp1',
                 hue='treatment', palette=pal, axes=axes,
                 xlabel='', ylabel='PTBP1 relative expression',
                 pvalues=None, showlegend=False,
                 legend_loc=(0, 1.02), dots_size=5, ylims=None,
                 hue_order=levels, xorder=levels, show_yticklabels=True,
                 dodge=False, legend_cols=2)
    add_panel_label(axes, label='C', xfactor=0.8)
    axes.set_xticklabels(levels, rotation=30, fontsize=9, ha='center')
    
    # Cardiac Mass
    axes = fig.add_subplot(gs[40:65, 40:60])
    plot_boxplot(echos, x='treatment', y='Normalized_MASS',
                 hue='treatment', palette=pal, axes=axes,
                 xlabel='', ylabel='Normalized cardiac mass',
                 pvalues=None, showlegend=False,
                 legend_loc=None, dots_size=5, ylims=None,
                 hue_order=levels, xorder=levels, show_yticklabels=True,
                 dodge=False, legend_cols=2)
    add_panel_label(axes, label='D', xfactor=0.6)
    axes.set_xticklabels(levels, rotation=30, fontsize=9, ha='center')
    
    # Cardiac Mass
    axes = fig.add_subplot(gs[40:65, 80:100])
    plot_boxplot(echos, x='treatment', y='MV_EoverA',
                 hue='treatment', palette=pal, axes=axes,
                 xlabel='', ylabel='Mitral Valve E/A ratio',
                 pvalues=None, showlegend=True,
                 legend_loc=(0.15, 1), dots_size=5, ylims=None,
                 hue_order=levels, xorder=levels, show_yticklabels=True,
                 dodge=False, legend_cols=1)
    add_panel_label(axes, label='E', xfactor=0.6)
    axes.set_xticklabels(levels, rotation=30, fontsize=9, ha='center')
    
    # Heart function qPCRs
    axes = fig.add_subplot(gs[80:110, :65])
    pal = {k: GROUP_PAL[k] for k in levels}
    markers = HEART_FUNC_MARKERS + FIBROSIS_MARKERS
    markers_upper = [x.upper() for x in markers]
    qpcrs_long = pd.melt(qpcrs[markers + ['treatment']], id_vars=['treatment'])
    qpcrs_long['variable'] = [x.upper() for x in qpcrs_long['variable']]
    plot_boxplot(qpcrs_long, x='variable', y='value',
                 hue='treatment', palette=pal, axes=axes,
                 xlabel='', ylabel='Relative expression',
                 legend_loc=(0.35, 1.05), dots_size=5, ylims=None,
                 pvalues=None, showlegend=False, panel_label='B',
                 hue_order=levels, xorder=markers_upper,
                 show_yticklabels=True, legend_cols=2)
    add_panel_label(axes, label='F', xfactor=0.19)
    axes.set_xticklabels(markers_upper, rotation=30, fontsize=9,
                         ha='center')
    
    # Histology
    axes = fig.add_subplot(gs[80:110, 80:])
    plot_boxplot(histology, x='treatment', y='perc',
                 hue='treatment', palette=pal, axes=axes,
                 xlabel='', ylabel='% fibrotic area',
                 pvalues=None, showlegend=False,
                 legend_loc=None, dots_size=5, ylims=None,
                 hue_order=levels, xorder=levels, show_yticklabels=True,
                 dodge=False, legend_cols=2)
    add_panel_label(axes, label='G', xfactor=0.8)
    axes.set_xticklabels(levels, rotation=30, fontsize=9, ha='center')
    
    # Save plot
    fpath = join(PLOTS_DIR, 'ptbp1_phenotype.png')
    fig.savefig(fpath, format='png', dpi=240)
    