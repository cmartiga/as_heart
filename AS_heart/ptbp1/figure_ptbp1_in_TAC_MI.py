#!/usr/bin/env python
from os.path import join

import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import pandas as pd

from scripts.settings import DATA_DIR, PLOTS_DIR, AS_CANDIDATES
from scripts.plot_utils import plot_boxplot, GROUP_PAL, add_panel_label


if __name__ == '__main__':
    tac_qpcrs = pd.read_csv(join(DATA_DIR, 'TAC_qpcrs.adjusted.csv'))
    mi_qpcr = pd.read_csv(join(DATA_DIR, 'MI.ptbp1_qpcr.csv'))
    
    # Init Figures
    fig = plt.figure(figsize=(7, 5))
    gs = GridSpec(100, 100)
    
    # TAC Ptbp1 expression
    axes = fig.add_subplot(gs[:45, :20])
    levels = ['Sham', 'TAC']
    pal = {k: GROUP_PAL[k] for k in levels}
    plot_boxplot(tac_qpcrs, x='treatment', y='ptbp1',
                 hue='treatment', palette=pal, axes=axes,
                 xlabel='', ylabel='PTBP1 relative expression',
                 pvalues=None, showlegend=False,
                 legend_loc=(0, 1.02), dots_size=5, ylims=None,
                 hue_order=levels, xorder=levels, show_yticklabels=True,
                 dodge=False, legend_cols=2)
    add_panel_label(axes, label='A', xfactor=0.6)
    
    # MI Ptbp1 expression
    axes = fig.add_subplot(gs[:45, 45:])
    levels = ['Control', 'Remote', 'Border', 'Infarct']
    pal = {k: GROUP_PAL[k] for k in levels}
    plot_boxplot(mi_qpcr, x='Group', y='ptbp1',
                 hue='Group', palette=pal, axes=axes,
                 xlabel='', ylabel='PTBP1 relative expression',
                 pvalues=None, showlegend=False,
                 legend_loc=(0, 1.02), dots_size=5, ylims=None,
                 hue_order=levels, xorder=levels, show_yticklabels=True,
                 dodge=False, legend_cols=4)
    add_panel_label(axes, label='B', xfactor=0.25)
    
    # TAC splicing validation
    axes = fig.add_subplot(gs[55:, :])
    levels = ['Sham', 'TAC']
    pal = {k: GROUP_PAL[k] for k in levels}
    markers = AS_CANDIDATES
    markers_upper = [x.upper() for x in markers]
    tac_qpcrs_long = pd.melt(tac_qpcrs[markers + ['treatment']], id_vars=['treatment'])
    tac_qpcrs_long['variable'] = [x.upper() for x in tac_qpcrs_long['variable']]
    plot_boxplot(tac_qpcrs_long, x='variable', y='value',
                 hue='treatment', palette=pal, axes=axes,
                 xlabel='', ylabel='Relative expression',
                 pvalues=None, showlegend=True, panel_label='C',
                 legend_loc=4, dots_size=5, ylims=(0, 1.75),
                 hue_order=levels, xorder=markers_upper,
                 show_yticklabels=True, legend_cols=4)
    add_panel_label(axes, label='C', xfactor=0.12)
    axes.set_xticklabels(markers_upper, rotation=45, fontsize=10,
                         ha='right')
    
    # Save plot
    fpath = join(PLOTS_DIR, 'ptbp1_validation_tac_mi.png')
    fig.savefig(fpath, format='png', dpi=240)
    