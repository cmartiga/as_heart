#!/usr/bin/env python
from os.path import join

import pandas as pd

from AS_heart.settings import DATA_DIR, PLOTS_DIR
from AS_heart.plot_utils import (plot_boxplot, GROUP_PAL,
                                 init_fig, savefig)


if __name__ == '__main__':
    echos = pd.read_csv(join(DATA_DIR, 'echo_data.adjusted.csv'))
    levels = ['AAV9-Control', 'AAV9-PTBP1']
    pal = {k: GROUP_PAL[k] for k in levels}
    
    # Init Figures
    fig, subplots = init_fig(1, 2, rowsize=2.8, colsize=2.5)
    axes = subplots[0]
    plot_boxplot(echos, x='treatment', y='SA_MM_EF',
                 hue='treatment', palette=pal, axes=axes,
                 xlabel='',
                 ylabel=r'SA-LV Ejection Fraction (%)',
                 pvalues=None, showlegend=False,
                 legend_loc=None, dots_size=5, ylims=None,
                 hue_order=levels, xorder=levels, show_yticklabels=True,
                 dodge=False, legend_cols=2)
    axes.set_xticklabels(levels, rotation=30, fontsize=10, ha='center')
    
    axes = subplots[1]
    plot_boxplot(echos, x='treatment', y='LA_2D__EF',
                 hue='treatment', palette=pal, axes=axes,
                 xlabel='',
                 ylabel=r'LA-LV Ejection Fraction (%)',
                 pvalues=None, showlegend=False,
                 legend_loc=None, dots_size=5, ylims=None,
                 hue_order=levels, xorder=levels, show_yticklabels=True,
                 dodge=False, legend_cols=2)
    axes.set_xticklabels(levels, rotation=30, fontsize=10, ha='center')
    
    
    # Save plot
    fpath = join(PLOTS_DIR, 'ptbp1_EF.png')
    savefig(fig, fpath)
    