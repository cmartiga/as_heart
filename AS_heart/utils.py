from os.path import join
import sys
from time import ctime

from scipy.stats._discrete_distns import hypergeom
from scipy.stats.stats import fisher_exact
from statsmodels.stats.multitest import multipletests
import statsmodels.formula.api as smf

import numpy as np
import pandas as pd
from AS_heart.settings import (EXT_DATA_DIR, DPSI_THRESHOLD, DAS_GROUPS, INCLUDED,
                              SKIPPED, UPREGULATED, DOWNREGULATED, QVAL, BETA,
                              DEG_FPATH, DAS, FUNCTIONS_DIR, DEG, NO_CHANGE,
                              DATA_DIR)
from _collections import defaultdict


GENE_SETS = {'chip': 'ENCODE_TF_ChIP',
             'go': 'GO_Biological_Process',
             'kegg': 'KEGG',
             'tf_pertubations': 'TF_Perturbations',
             'tf_ppis': 'Transcription_Factor_PPIs',
             'tf_motifs': 'TRANSFAC_and_JASPAR_PWMs',
             'mirna': 'TargetScan'}
FILTER_GENE_SETS = {'miRNA': lambda x: 'mmu' in x,
                    'TFmotifs': lambda x: 'mouse' in x,
                    'ChIP': lambda x: 'mm9' in x}


class LogTrack(object):
    '''Logger class'''
    def __init__(self, fhand=sys.stderr):
        self.fhand = fhand

    def init(self, dataset_dir, script_name):
        log_fpath = join(dataset_dir, '{}.log'.format(script_name))
        self.fhand = open(log_fpath, 'w')
        self.write('Start')

    def write(self, msg, add_time=True):
        if add_time:
            msg = '[ {} ] {}\n'.format(ctime(), msg)
        else:
            msg += '\n'
        self.fhand.write(msg)
        self.fhand.flush()

    def finish(self):
        self.write('Finished succesfully')


def parse_coordinates(coords_str):
    chrom, coords = coords_str.split(':')
    start, end = [int(x) for x in coords.split('-')]
    return(chrom, start, end)


def load_gmt(fpath, expressed, min_size=5, ignore_source=True,
             filter_cols=None):
    data = defaultdict(dict)
    for line in open(fpath):
        items = line.strip().split('\t')
        key_value = items[0]
        if filter_cols is not None and not filter_cols(key_value):
            continue

        if ignore_source:
            key_value = key_value.split('_')[0]
        data[key_value].update({item: 1 for item in items[1:]})
    data = pd.DataFrame(data)
    sel_rows = np.intersect1d(expressed, data.index)
    data = data.loc[sel_rows, :].fillna(0)
    data = data.loc[:, data.sum(0) > min_size]
    data = data.T.drop_duplicates().T
    return(data)


def load_ids2symbols():
    # Load gene symbols: downloaded from biomart
    fpath = join(EXT_DATA_DIR, 'ids2symbols.csv')
    ids2symbols = dict([tuple(line.strip().split(','))
                        for line in open(fpath)])
    return(ids2symbols)


def load_expression(ids2symbols):
    # Load 
    deg = pd.read_csv(DEG_FPATH, index_col=0, sep='\t', decimal=',')

    # Set gene names as unique keys
    deg['gene_name'] = [ids2symbols.get(gene_id, gene_id).upper()
                        for gene_id in deg.index]
    deg['gene_id'] = deg.index
    deg.drop_duplicates('gene_name', inplace=True, keep=False)
    deg.set_index('gene_name', inplace=True)
    return(deg)


def add_deg_categories(deg, qval_threshold=0.05, beta_threshold=1):
    up = np.logical_and(deg[QVAL] < qval_threshold,
                        deg[BETA] > beta_threshold)
    dw = np.logical_and(deg[QVAL] < qval_threshold,
                        deg[BETA] < -beta_threshold)
    deg[UPREGULATED] = up.astype(int)
    deg[DOWNREGULATED] = dw.astype(int)
    deg[DEG] = np.logical_or(up, dw).astype(int)
    return(deg)


def load_vast_splicing():
    fpath = join(DATA_DIR, 'differential_splicing.tsv')
    splc_data = pd.read_csv(fpath, index_col=False, sep='\t')
    splc_data.columns = ['gene_name', 'event_id', 'Control',
                         'PTBP1', 'DeltaPsi', 'diff0.95']
    splc_data['DeltaPsi'] = splc_data['PTBP1'] - splc_data['Control']
    splc_data['gene_name'] = [str(gene_name).upper()
                              for gene_name in splc_data['gene_name']]
    splc_data.index = splc_data['event_id']
    return(splc_data)


def add_event_types_vast(splc_data):
    event_types = {'MmuEX00': 'CE', 'MmuINT0': 'IR',
                   'MmuALTD': 'AD', 'MmuALTA': 'AA'}
    splc_data['Type'] = [event_types.get(event_id[:7], None)
                         for event_id in splc_data['event_id']]
    return(splc_data)


def add_das_categories(splc_data, dpsi_threshold=DPSI_THRESHOLD,
                       p_threshold=0):
    inc = np.logical_and(splc_data['DeltaPsi'] > dpsi_threshold,
                         splc_data['diff0.95'] > p_threshold)
    skp = np.logical_and(splc_data['DeltaPsi'] < -dpsi_threshold,
                         splc_data['diff0.95'] > p_threshold)

    splc_data['Group'] = NO_CHANGE
    splc_data.loc[inc, 'Group'] = INCLUDED
    splc_data.loc[skp, 'Group'] = SKIPPED

    splc_data[INCLUDED] = inc.astype(int)
    splc_data[SKIPPED] = skp.astype(int)
    splc_data[DAS] = np.logical_or(inc, skp).astype(int)
    return(splc_data)


def get_splc_gene_groups(splc_data, by='Type', groups=DAS_GROUPS):
    splc_gene = []
    for group in groups:
        group_data = splc_data.groupby([by, 'gene_name'])[group].any()
        group_data = group_data.reset_index()
        group_data['Group'] = group
        group_data.columns = [by, 'gene', 'change', 'Group']
        group_data['key'] = ['{}_{}'.format(x, group) for x in group_data[by]]
        splc_gene.append(group_data)
    splc_gene = pd.concat(splc_gene)
    splc_gene = pd.pivot_table(data=splc_gene, values='change', index='gene',
                               columns='key', fill_value=False, aggfunc=any)
    return(splc_gene.astype(int))


def multiple_test_correction(results, field='pvalue', name='fdr'):
    if results.shape[0] == 1:
        results[name] = results[field]
    else:
        results[name] = np.nan
        sel_rows = np.isnan(results[field]) == False
        res = multipletests(results.loc[sel_rows, field], method='fdr_bh')
        results.loc[sel_rows, name] = res[1]


def load_gene_sets(db, genes):
    if db not in GENE_SETS:
        raise ValueError('Gene sets {} are not available'.format(db))
    fpath = join(EXT_DATA_DIR, GENE_SETS[db])
    gene_sets = load_gmt(fpath, genes, ignore_source=True,
                         filter_cols=FILTER_GENE_SETS.get(db, None))
    return(gene_sets)


def hypergeom_test(group1, group2):
    M = group1.shape[0]
    N = group2.sum()
    n = group1.sum()
    x = np.logical_and(group1 == 1, group2 == 1).sum()
    pvalue = hypergeom.cdf(x, M, n, N)
    logors = np.log(x * M / (n * N))
    print([[x, N], [n, M]])
    return(logors, pvalue)


def calc_contingency_table(x, y):
    a = np.logical_and(x == 1, y == 1).sum()
    b = np.logical_and(x == 1, y == 0).sum()
    c = np.logical_and(x == 0, y == 1).sum()
    d = np.logical_and(x == 0, y == 0).sum()
    return(np.array([[a, c], [b, d]]))


def enrichment_analysis(gene_sets, gene_groups, group):
    variables = gene_sets.columns
    groups = set([x for x in gene_groups.columns])
    pvalues = []
    estimates = []
    for var in variables:
        if group not in groups:
            ors, pvalue = 1, 1
        else:
            table = calc_contingency_table(gene_groups[group], gene_sets[var])
            ors, pvalue = fisher_exact(table, alternative='greater')
        pvalues.append(pvalue)
        estimates.append(np.log(ors))

#         model = sm.GLM(gene_sets[var], gene_grous[['intercept', group]],
#                        family=sm.families.Binomial())
#         results = model.fit()
#         pvalues.append(results.pvalues[1])
#         estimates.append(results.params[1])

    results = pd.DataFrame({'pvalue': pvalues, 'logors': estimates},
                           index=gene_sets.columns)

    # Multiple test correction
    multiple_test_correction(results)
    return(results)


def load_functional_enrichment(db, gene_groups, topterms=10,
                               readjust_pval=False):
    enrich_results = []
    selected_sets = []
    for group in gene_groups:
        fpath = join(FUNCTIONS_DIR, '{}.ORA_{}.csv'.format(db, group))
        group_enrichment = pd.read_csv(fpath, index_col=0).sort_values('fdr')

        # Remove GO identifier if present
        group_enrichment['term'] = [x.split('(')[0]
                                    for x in group_enrichment.index]

        group_enrichment['Group'] = group
        selected_sets.extend(group_enrichment.index[:topterms])
        enrich_results.append(group_enrichment)

    selected_sets = np.unique(selected_sets)
    enrich_results = pd.concat([group_enrichment.loc[np.intersect1d(selected_sets, group_enrichment.index), :]
                                for group_enrichment in enrich_results])
    if readjust_pval:
        multiple_test_correction(enrich_results)

    enrich_results['logfdr'] = -np.log10(enrich_results['fdr'])
    return(enrich_results)


def get_enrich_logfdr_matrix(enrich_results, columns='Group', colnames=None):
    logfdr = pd.pivot_table(enrich_results, values='logfdr',
                            index='term', columns=columns, fill_value=0)
    if colnames is not None:
        logfdr.columns = colnames

    logfdr['term'] = logfdr.index
    return(logfdr)


def perform_statsitical_analysis(data, variables, batch_correction=True):
    batch_adjusted = {'treatment': data['treatment']}
    stat_results = []
    for variable in variables:
        if batch_correction:
            formula='{} ~ treatment + batch'.format(variable)
        else:
            formula='{} ~ treatment'.format(variable)
        model = smf.glm(formula=formula, data=data)
        results = model.fit()
        estimate, pvalue = results.params, results.pvalues
        
        batch_adjusted[variable] = data[variable]
        if batch_correction:
            batch_adjusted[variable] = data[variable] - estimate[2] * (data['batch'] == '2')
            
        record = {'Parameter': variable, 'Control': estimate[0],
                  'PTBP1': estimate[0] + estimate[1], 'P-value': pvalue[1]}
        stat_results.append(record)
    stat_results = pd.DataFrame(stat_results)
    batch_adjusted = pd.DataFrame(batch_adjusted)
    return(stat_results, batch_adjusted)
