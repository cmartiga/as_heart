from os.path import join, abspath, dirname

VERSION = '0.1.0'

# Directories
BASE_DIR = abspath(join(dirname(__file__), '..'))
DATA_DIR = join(BASE_DIR, 'data')
EXT_DATA_DIR = join(DATA_DIR, 'external_data')
CLIP_DIR = join(DATA_DIR, 'events_clip')
PLOTS_DIR = join(BASE_DIR, 'plots')
FUNCTIONS_DIR = join(DATA_DIR, 'functional_analysis')

# File paths
DESIGN_FPATH = join(DATA_DIR, 'design.csv')
CLIP_FPATH = join(EXT_DATA_DIR, 'ptbp1.clip.mm9.bed.gz')
DEG_FPATH = join(DATA_DIR, 'differential_expression.tsv')
DAS_FPATH = join(DATA_DIR, 'differential_splicing.tsv')

MOUSE_ORTHOLOGS_FPATH = join(EXT_DATA_DIR, 'human_mouse_orthologs.csv')
EVENT2GENE_FPATH = join(EXT_DATA_DIR, 'event_id.gene_symbol.tsv')
EVENT2GENEID_FPATH = join(EXT_DATA_DIR, 'event_id.gene_id.tsv')
ID2SYMBOL_FPATH = join(EXT_DATA_DIR, 'gene_ids.gene_symbols.tsv')

EXONS_DOMAINS_FPATH = join(EXT_DATA_DIR, 'VASTDB_PROT_PFAM_Mmu139_mm9.tab')
MOUSE_PFAM_FPATH = join(EXT_DATA_DIR, 'mouse_pfam.txt')
HUMAN_PFAM_FPATH = join(EXT_DATA_DIR, 'human_pfam.txt')
PFAM_INTERACTIONS_FPATH = join(EXT_DATA_DIR, 'ipfam_domain_interactions.txt')
DDI_INTERACTIONS_FPATH = join(EXT_DATA_DIR, 'intact_domain_interactions.csv')
VIDAL_PPI_FPATH = join(EXT_DATA_DIR, 'vidal_ppi.csv')
INTACT_FPATH = join(EXT_DATA_DIR, 'mouse.intact.tsv')

# fixed parameters
QVAL_THRESHOLD = 0.1
BETA_THRESHOLD = 0
DPSI_THRESHOLD = 0.1
P_THRESHOLD = 0.1

# GE names
QVAL = 'adj.P.Val'
BETA = 'logFC'
UPREGULATED = 'Upregulated'
DOWNREGULATED = 'Downregulated'
DEG = 'DEG'
DEG_GROUPS = [UPREGULATED, DOWNREGULATED]

# AS names
EVENT_TYPES_VAST = ['CE'] # ['AD', 'AA', 'CE', 'IR']
INCLUDED = 'Included'
SKIPPED = 'Skipped'
NO_CHANGE = 'No change'
DPSI = 'E[dPsi]'
DAS = 'DAS'
DAS_GROUPS = [INCLUDED, SKIPPED]

# other names
SETS_DBS = ['go', 'kegg']
SETS_LABELS = {'go': 'GO biological processes',
               'kegg': 'KEGG pathways'}

RESULTS_DIR = join(DATA_DIR, 'results')
DESIGN_FPATH = join(DATA_DIR, 'samples_references.csv')

# Region names
CLIP = 'clip'
UPSTREAM = 'Upstream'
DOWNSTREAM = 'Downstream'
NODE = 'Node'
REGIONS = [UPSTREAM, DOWNSTREAM, NODE]
DEF_REGIONS = ['Upstream_exon.r.50', 'Upstream_intron.l.250',
               'Upstream_intron.r.250', 'Alternative_exon.l.50',
               'Alternative_exon.r.50', 'Downstream_intron.l.250',
               'Downstream_intron.r.250', 'Downstream_exon.l.50']

# Other
AS_CANDIDATES = ['dpf2', 'vav2', 'ganab', 'pdlim7', 'exoc7', 'dst', 'h13',
                 'agap3', 'ap2a1', 'wnk1']
HEART_FUNC_MARKERS = ['bnp', 'myh7', 'anf']
FIBROSIS_MARKERS = ['lox', 'col3a1', 'col1a1']
CONDITIONS = ['Embryo', 'Neonate', 'Adult', 'TAC', 'MI']
CONTRASTS = ['ED', 'PD', 'TAC', 'MI']
SP_PREFIX = 'ENSMUSG'
