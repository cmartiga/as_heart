# Differential Gene expression
Rscript R_scripts/expression_analysis.R

# Perform statistical analysis and batch corrections
python scripts/analysis_ptbp1_aav9.py

# Perform functional enrichment analysis
python scripts/functional_analysis.py

# Make figures
python scripts/figure_ptbp1_in_TAC_MI.py
python scripts/figure_ptbp1_oe.py
python scripts/figure_ptbp1_rnaseq.py
python scripts/figure_functional_analysis.py
python scripts/figure_profile.py